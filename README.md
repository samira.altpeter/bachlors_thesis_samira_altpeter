# Bachlors Thesis Samira Altpeter

## Abstract
This thesis concerns the development of a photocurrent in a electronic ferroelectric phase. The bulk photovoltaic effect is examined and here I focus on systems with only electron-electron interactions and no phonons. To model the electronic ferroelectricity I used the Falicov-Kimball model, and to deduce the ground state of the system and to perform the time evolution I used the Lanczos algorithm. To model the photoexcitation a quench and a semi-classical approach via the Peierls substitution are used. I observed for both excitations a development of a photocurrent, which is enhanced by the ferroelectric phase. For the Peierls substitution a respond of the system appeared, which can be interpreted as a ballistic current. However, the examined currents are not directional. In addition, calculations with larger numbers of lattice sites should be performed to verify my results.


## Structure of this git respoitory
In the directory Python_Code the Python files and the generated figures are shown. During the work on my Bachlor's Thesis I pushed into the Git repository https://gitlab.gwdg.de/samira.altpeter/bachelorarbeit.git at intermittent intervals. If you want to have access to that repository, you can ask my supervisor Dr. Salvatore R. Manmana for permission.


### Functions.py
In this Python file the most important functions for my thsis are defined. It contains the used Hamiltonians, numerical methods and the used operators.


### Usage of the executable python files
In the first lines of each file the necessary moduls and functions from the Functions.py are imported. Here, one has to change the path to the Functions.py in line 3 to the corresponding path to the directory of the Functions.py. 

After the imports, the modifiable paramters of the file are shown. These parameters can be changed to the desired values. Note that the number of lattice sites L for the Falicov-Kimball model in the Python files is the one for the mapped Hamiltonian. So this L corresponds to the \Tilde{L} in my thesis. The parameter nf in the Python files corresponds to the particle density n from my thesis.

After the parameters, the executable code is displayed. Here the path of the files to save the generated data has to be changed to your preferred directory.



