import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear
from Functions import Local_fermion_number_linear, Charge_location_Polar_linear, Polarization_FKM_linear

import numpy as np
import matplotlib.pyplot as plt

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1.
t_d = 1. # unit of energy
t_f = -0.1

U = -7.5
V = 4.


L = 20 # Number of lattice sites

# Particle density:
nf = 0.7

k = 140 # Number of Lanczos iteration steps

#%% Local density

H, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U, V, nf)

# Complete Diagonalization for comparisson:
#Energies, Vectors = np.linalg.eigh(H.toarray())

# Test vector:
phi = np.ones(n)
phi_0 = phi/np.linalg.norm(phi)

E, V_matrix, Q_T = lanczos_iter(H, phi_0, k)


# Ground state of the system in the original basis:
state = lin_comb_Q_T(V_matrix[:,0], Q_T)

density_d, density_f = Local_fermion_number_linear(L, nf, state)

L_lst = np.arange(0, L/2, 1)



# Plot:

plt.figure(figsize=(9, 7))

plt.ylabel("Local density $<n_i>$", size = 18)
plt.xlabel("Lattice site $i$", size = 18)

plt.scatter(L_lst, density_d, color='green', label = "d-fermions")
plt.plot(L_lst, density_d, color='green')

plt.scatter(L_lst, density_f, color='brown', label = "f-fermions")
plt.plot(L_lst, density_f, color='brown')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_FKM_local_num_nf_{}_L_{}_U_{}_V_{}.pdf'.format(nf, L, U, V))

plt.show()



#print("Energie ED", Energies[0])
print("Energie Lanczos", E[0])
#print("Vector ED", Vectors[:,0])
print("Vector Lanczos", state)

