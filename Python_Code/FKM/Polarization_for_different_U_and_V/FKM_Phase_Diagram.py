import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear, Polarization_FKM_linear, Charge_location_Polar_linear


import numpy as np
import matplotlib.pyplot as plt

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter


import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1.
t_d = 1. # unit of energy
t_f = -0.1

del_elements_V = 0.25
del_elements_U = 0.25
U = np.arange(-10, 10 + del_elements_U, del_elements_U)
V = np.arange(-4, 4 + del_elements_V, del_elements_V)


# Particle density:
nf = 0.7

# Number of lattice sites:
L = 20

# Number of Lanczos iteration steps:
k = 140 

#%% Phase diagram for Polarization

# Files for saving the data:
file_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Phase_Diagram/P1.txt", 'w')
file_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Phase_Diagram/P2.txt", 'w')


# Length of the U and V arrays:
num_elements_U = len(U)
num_elements_V = len(V)


# Inverse of 'real' number of lattice sites:
L_real_inv = 2/L


# Auxiliary lists and variables:
Polarization = np.zeros((num_elements_U, num_elements_V))
Charge_loc = np.zeros((num_elements_U, num_elements_V))
U_count = 0
V_count = 0


print("Begin calculation of polarization")

for i in range(num_elements_U):
    U_count += 1

    for j in range(num_elements_V):
        V_count += 1
        print("U-step {} and V-step {}".format(U_count, V_count))

        # Operators:
        P = Polarization_FKM_linear(L, nf)
        ch_loc_op = Charge_location_Polar_linear(L, nf)

        # Hamiltonian:
        H, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U[i], V[j], nf)

        # Test vector:
        phi = np.ones(n)
        phi_0 = phi/np.linalg.norm(phi)
            
        E, V_matrix, Q_T = lanczos_iter(H, phi_0, k)
            
        # Ground state of the system in the original basis:
        state = lin_comb_Q_T(V_matrix[:,0], Q_T)
            
        # Calculation of the expectation values:
        Polarization[i,j] = np.conj(state).dot(P.dot(state))
        Charge_loc[i, j] = np.conj(state).dot(ch_loc_op.dot(state))
        
        
        file_1.write('{}\t {}\t {}\n'.format(U[i], V[j], Polarization[i,j]))
        file_2.write('{}\t {}\t {}\n'.format(U[i], V[j], Charge_loc[i,j]))
        
        
        print("Polarization saved")
    
    V_count = 0


file_1.close()
file_2.close()


# Plot:
    
V_grid_1, U_grid_1 = np.meshgrid(V, U)
levels_1 = np.linspace(Polarization.min(), Polarization.max(), 15)

plt.xlabel('Hybridization $V$')
plt.ylabel('Coulomb interaction $U$')

CS = plt.contourf(V_grid_1, U_grid_1, Polarization, levels = levels_1)

cbar = plt.colorbar(CS)
cbar.ax.set_ylabel('Polarization $P_1$')

plt.savefig(my_path + '_FKM_phase_diagram_polarization_nf_{}_L_{}.pdf'.format(nf, L))

plt.show()


V_grid_2, U_grid_2 = np.meshgrid(V, U)
levels_2 = np.linspace(Charge_loc.min(), Charge_loc.max(), 15)

plt.xlabel('Hybridization $V$')
plt.ylabel('Coulomb interaction $U$')

CS = plt.contourf(V_grid_2, U_grid_2, Charge_loc, levels = levels_2)

cbar = plt.colorbar(CS)
cbar.ax.set_ylabel('Polarization $P_2$')

plt.savefig(my_path + '_FKM_phase_diagram_ch_loc_nf_{}_L_{}.pdf'.format(nf, L))

plt.show()


