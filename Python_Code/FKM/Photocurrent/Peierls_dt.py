import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear, Time_evolution_Lanczos_opt
from Functions import current_d, current_f, current_df, local_current
from Functions import Local_fermion_number_linear


import numpy as np
import matplotlib.pyplot as plt

import timeit

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1.
t_d = 1. # unit of energy
t_f = -0.1
V = 4.
U = -7.5


# Particle density:
nf = 0.7

# Number of lattice sites:
L = 20

# Number of iteration steps for the Lanczos algorithm:
k = 140 
k_t = 20

# Considered time:
dt_1 = 0.01 # Time step
dt_2 = 0.005 # Time step
t_list_1 = np.arange(0, 80, dt_1)
t_list_2 = np.arange(0, 80, dt_2)


# Variables of the Peierls phase:
t_0 = 10.
A_0 = 1.
sigma = 1.7
omega = 4.

exp_on_off = 1 # Factor to turn the gaussian part of the Peierls on or off

#%% Current with Peierls phase

# Files for saving the data:
file_j_d_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_dt1_j_d_L_{}_U_{}.txt".format(L, U), 'w')
file_j_f_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_dt1_j_f_L_{}_U_{}.txt".format(L, U), 'w')
file_j_df_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_dt1_j_df_L_{}_U_{}.txt".format(L, U), 'w')

# Files for saving the data:
file_j_d_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_dt2_j_d_L_{}_U_{}.txt".format(L, U), 'w')
file_j_f_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_dt2_j_f_L_{}_U_{}.txt".format(L, U), 'w')
file_j_df_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_dt2_j_df_L_{}_U_{}.txt".format(L, U), 'w')


# Number of time steps:
n_times_1 = len(t_list_1)
n_times_2 = len(t_list_2)


# Hamiltonian for t = 0:
print("Start calculation of the ground state")
H_t0, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U, V, nf)

# Test vector:
phi = np.ones(n)
phi_0 = phi/np.linalg.norm(phi)

E, V_matrix, Q_T = lanczos_iter(H_t0, phi_0, k)

# Ground state of the system in the original basis:
state_t0_1 = lin_comb_Q_T(V_matrix[:,0], Q_T)

state_t0_2 = lin_comb_Q_T(V_matrix[:,0], Q_T)

print("End calculation of the ground state")


# Peierls phase time evolution:
print("Start of time evolution")

# Pump pulse:
def Peierls_phase_pump_pulse_d(times, t_0, A_0, sigma, omega, t_d, exp_on_off):
    """
    ------------------
    times: Considered time
    t_0: Center of the pump pulse
    A_0: Amplitude of the pump pulse
    sigma: Width of the pump pulse
    omega: Frequency of the pump pulse
    t_d: Hopping amplitude of the d-electrons
    ------------------
    """
    A = A_0 * np.exp(-exp_on_off * (times - t_0)**2 / (2 * sigma**2)) * np.cos(omega * (times - t_0))
    return t_d * np.exp(1j * A)

def Peierls_phase_pump_pulse_f(times, t_0, A_0, sigma, omega, t_f, exp_on_off):
    """
    ------------------
    times: Considered time
    t_0: Center of the pump pulse
    A_0: Amplitude of the pump pulse
    sigma: Width of the pump pulse
    omega: Frequency of the pump pulse
    t_f: Hopping amplitude of the f-electrons
    ------------------
    """
    A = A_0 * np.exp(-exp_on_off * (times - t_0)**2 / (2 * sigma**2)) * np.cos(omega * (times - t_0))
    return t_f * np.exp(1j * A)


# Operators for the currents:
j_d_op = current_d(t_d, L, nf).tocsr()
j_f_op = current_f(t_f, L, nf).tocsr()
j_df_op = current_df(V, L, nf).tocsr()

# List for the global current:
j_d_1 = []
j_f_1 = []
j_df_1 = []
    
j_d_2 = []
j_f_2 = []
j_df_2 = []


start_time_evolution = timeit.default_timer()

for i in range(n_times_1):
    print("Step of time evolution: ", i + 1)
    # Calculation of the Hamiltonian:
    Peierls_d = Peierls_phase_pump_pulse_d(i * dt_1, t_0, A_0, sigma, omega, t_d, exp_on_off)
    Peierls_f = Peierls_phase_pump_pulse_f(i * dt_1, t_0, A_0, sigma, omega, t_f, exp_on_off)


    H_t, n = Hamiltonian_FKM_linear(L, Peierls_d, Peierls_f, epsilon_d, epsilon_f, U, V, nf)
    
    H_sparse = H_t.tocsr()
    
    # Lanczos time evolution:
    state_t = Time_evolution_Lanczos_opt(k_t, H_sparse, state_t0_1, dt_1, n)
    
    
    # Calculation of the expectation values:
    j_d_exp = np.conj(state_t).dot(j_d_op.dot(state_t))
    j_d_1.append(j_d_exp)

    j_f_exp = np.conj(state_t).dot(j_f_op.dot(state_t))
    j_f_1.append(j_f_exp)
 
    j_df_exp = np.conj(state_t).dot(j_df_op.dot(state_t))
    j_df_1.append(j_df_exp)

    file_j_d_1.write('{}\t {}\n'.format(i*dt_1, j_d_1[i]))
    file_j_f_1.write('{}\t {}\n'.format(i*dt_1, j_f_1[i]))
    file_j_df_1.write('{}\t {}\n'.format(i*dt_1, j_df_1[i]))

    state_t0_1 = state_t
    
    
for i in range(n_times_2):
    print("Step of time evolution: ", i + 1)
    # Calculation of the Hamiltonian:
    Peierls_d = Peierls_phase_pump_pulse_d(i * dt_2, t_0, A_0, sigma, omega, t_d, exp_on_off)
    Peierls_f = Peierls_phase_pump_pulse_f(i * dt_2, t_0, A_0, sigma, omega, t_f, exp_on_off)


    H_t, n = Hamiltonian_FKM_linear(L, Peierls_d, Peierls_f, epsilon_d, epsilon_f, U, V, nf)
    
    H_sparse = H_t.tocsr()
    
    # Lanczos time evolution:
    state_t = Time_evolution_Lanczos_opt(k_t, H_sparse, state_t0_2, dt_2, n)
    
    
    # Calculation of the expectation values:
    j_d_exp = np.conj(state_t).dot(j_d_op.dot(state_t))
    j_d_2.append(j_d_exp)

    j_f_exp = np.conj(state_t).dot(j_f_op.dot(state_t))
    j_f_2.append(j_f_exp)

    j_df_exp = np.conj(state_t).dot(j_df_op.dot(state_t))
    j_df_2.append(j_df_exp)

    file_j_d_2.write('{}\t {}\n'.format(i*dt_2, j_d_2[i]))
    file_j_f_2.write('{}\t {}\n'.format(i*dt_2, j_f_2[i]))
    file_j_df_2.write('{}\t {}\n'.format(i*dt_2, j_df_2[i]))

    state_t0_2 = state_t
    
    
stop_time_evolution = timeit.default_timer()    
runtime_time_evolution = stop_time_evolution - start_time_evolution       
        
print("Runtime of time evolution: ", runtime_time_evolution)
      
print("End of time evolution")

file_j_d_1.close()
file_j_f_1.close()
file_j_df_1.close()

file_j_d_2.close()
file_j_f_2.close()
file_j_df_2.close()


# Calculation of global current:
j_global_1 = [j_d_1[i] + j_f_1[i] + j_df_1[i] for i in range(n_times_1)]
j_global_2 = [j_d_2[i] + j_f_2[i] + j_df_2[i] for i in range(n_times_2)]


summe_j_offset_1 = 0
j_int_list_1 = []

summe_j_offset_2 = 0
j_int_list_2 = []

for i in range(n_times_1):
    summe_j_offset_1 += j_global_1[i]
    
    j_int_list_1.append(summe_j_offset_1 / i)
  
    
for i in range(n_times_2):
    summe_j_offset_2 += j_global_2[i]
    
    j_int_list_2.append(summe_j_offset_2 / i)    
  
current_offset_1 = summe_j_offset_1 / n_times_1
current_offset_2 = summe_j_offset_2 / n_times_2
   


# Plots:

plt.figure(figsize=(9, 7))

plt.ylabel("Total current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list_1, j_global_1, color='green', label = '$dt = 0.05$', zorder = 1)
plt.plot(t_list_2, j_global_2, color='orange', label = '$dt = 0.01$', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_Peierls_global_current_verg_dt_nf_{}_U_{}_V_{}_A_{}.pdf'.format(nf, U, V, A_0))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("Integrated current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list_1, j_int_list_1, color='green', label = '$dt = 0.05$', zorder = 1)
plt.plot(t_list_2, j_int_list_2, color='orange', label = '$dt = 0.01$', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_Peierls_int_current_verg_dt_nf_{}_U_{}_V_{}_A_{}.pdf'.format(nf, U, V, A_0))

plt.show()


#%% Calculation integrated current

print("Current offset dt = 0.05: ", current_offset_1)
print("Current offset dt = 0.01: ", current_offset_2)
    
    