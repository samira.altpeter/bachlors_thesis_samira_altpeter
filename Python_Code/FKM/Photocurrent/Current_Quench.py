import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear, Time_evolution_Lanczos_opt
from Functions import current_d, current_f, current_df, local_current
from Functions import Local_fermion_number_linear


import numpy as np
import matplotlib.pyplot as plt

import timeit

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1.
t_d = 1. # unit of energy
t_f = -0.1
V = 4.


U_t0 = -7.5 # U for t = 0
U_t = 0. # U for t > 0


# Particle density:
nf = 0.7
 
# Number of lattice sites:
L = 20

# Number of iteration steps for the Lanczos algorithm:
k = 140 
k_t = 20

# Considered time:
dt = 0.01 # Time step
t_list = np.arange(0, 80, dt)

# List position of the considered time steps for the local current:
n_local_1 = 200
n_local_2 = 300

#%% Current with Quench

# Files for saving the data:
file_j_d = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_j_d_L_{}_U_{}.txt".format(L, U_t0), 'w')
file_j_f = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_j_f_L_{}_U_{}.txt".format(L, U_t0), 'w')
file_j_df = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_j_df_L_{}_U_{}.txt".format(L, U_t0), 'w')


# Number of time steps:
n_times = len(t_list)

# Hamiltonian for t = 0:
print("Start calculation of the ground state")
H_t0, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U_t0, V, nf)

# Test vector:
phi = np.ones(n)
phi_0 = phi/np.linalg.norm(phi)

E, V_matrix, Q_T = lanczos_iter(H_t0, phi_0, k)

# Ground state of the system in the original basis:
state_t0 = lin_comb_Q_T(V_matrix[:,0], Q_T)
print("End calculation of the ground state")


# Quench of U:
print("Start of time evolution")

# Operators for the currents:
j_d_op = current_d(t_d, L, nf).tocsr()
j_f_op = current_f(t_f, L, nf).tocsr()
j_df_op = current_df(V, L, nf).tocsr()


# List for the global current:
j_d = []
j_f = []
j_df = []


start_time_evolution = timeit.default_timer()

for i in range(n_times):
    print("Step of time evolution: ", i + 1)
    # Calculation of the Hamiltonian:
       
    H_t, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U_t, V, nf)    

    H_sparse = H_t.tocsr()
    
    # Lanczos time evolution:
    state_t = Time_evolution_Lanczos_opt(k_t, H_sparse, state_t0, dt, n)
    
    
    # Calculation of the expectation values:
    j_d_exp = np.conj(state_t).dot(j_d_op.dot(state_t))
    j_d.append(j_d_exp)
    
    j_f_exp = np.conj(state_t).dot(j_f_op.dot(state_t))
    j_f.append(j_f_exp)
    
    j_df_exp = np.conj(state_t).dot(j_df_op.dot(state_t))
    j_df.append(j_df_exp)
    
    
    # Calculation of local current:
    if(i == n_local_1):
        j_local_d_1, j_local_f_1, j_local_df_1 = local_current(t_d, t_f, V, L, nf, state_t)
        
        density_d_1, density_f_1 = Local_fermion_number_linear(L, nf, state_t)
        
    if(i == n_local_2):
        j_local_d_2, j_local_f_2, j_local_df_2 = local_current(t_d, t_f, V, L, nf, state_t)
        
        density_d_2, density_f_2 = Local_fermion_number_linear(L, nf, state_t)
        
    if(i == n_times - 1):
        density_d_final, density_f_final = Local_fermion_number_linear(L, nf, state_t)

    file_j_d.write('{}\t {}\n'.format(i*dt, j_d[i]))
    file_j_f.write('{}\t {}\n'.format(i*dt, j_f[i]))
    file_j_df.write('{}\t {}\n'.format(i*dt, j_df[i]))
    
    
    state_t0 = state_t
    
stop_time_evolution = timeit.default_timer()    
runtime_time_evolution = stop_time_evolution - start_time_evolution       
        
print("Runtime of time evolution: ", runtime_time_evolution)

print("End of time evolution")

file_j_d.close()
file_j_f.close()
file_j_df.close()


# Calculation of global current:
j_global = [j_d[i] + j_f[i] + j_df[i] for i in range(n_times)]
   
summe_j_offset = 0
summe_d = 0
summe_f = 0
j_int_list = []

for i in range(n_times):
    summe_j_offset += j_global[i]
    summe_d += j_d[i]
    summe_f += j_f[i]
    
    j_int_list.append(summe_j_offset / i)
    
current_offset = summe_j_offset / n_times
d_offset = summe_d /n_times
f_offset = summe_f /n_times


# Plots:

plt.figure(figsize=(9, 7))

plt.ylabel("$j_d$", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list, j_d, color='green', label = 'Integrated current', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_d_current_L_{}_nf_{}_V_{}_tf_{}_Ef_{}.pdf'.format(L, nf, V, t_f, epsilon_f))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("$j_f$", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list, j_f, color='green', label = 'Current', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_f_current_L_{}_nf_{}_V_{}_tf_{}_Ef_{}.pdf'.format(L, nf, V, t_f, epsilon_f))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("$j_{df}$", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list, j_df, color='green', label = 'Current', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_df_current_L_{}_nf_{}_V_{}_tf_{}_Ef_{}.pdf'.format(L, nf, V, t_f, epsilon_f))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("Total current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.axhline(current_offset, label = 'Integrated current', color='blue', zorder = 0)

plt.plot(t_list, j_global, color='green', label = 'Current', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_global_current_L_{}_nf_{}_V_{}_tf_{}_Ef_{}.pdf'.format(L, nf, V, t_f, epsilon_f))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("Integrated current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list, j_int_list, color='green', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_int_current_L_{}_nf_{}_V_{}_tf_{}_Ef_{}.pdf'.format(L, nf, V, t_f, epsilon_f))

plt.show()



L_lst = np.arange(0, L/2, 1)


plt.figure(figsize=(9, 7))

plt.ylabel("Local density $<n_i>$", size = 18)
plt.xlabel("Lattice site $i$", size = 18)

plt.scatter(L_lst, density_d_1, color='green', label = "d-fermions")
plt.plot(L_lst, density_d_1, color='green')

plt.scatter(L_lst, density_f_1, color='brown', label = "f-fermions")
plt.plot(L_lst, density_f_1, color='brown')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_FKM_local_num_nf_{}_L_{}_V_{}_nloc_{}.pdf'.format(nf, L, V, n_local_1))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("Local density $<n_i>$", size = 18)
plt.xlabel("Lattice site $i$", size = 18)

plt.scatter(L_lst, density_d_2, color='green', label = "d-fermions")
plt.plot(L_lst, density_d_2, color='green')

plt.scatter(L_lst, density_f_2, color='brown', label = "f-fermions")
plt.plot(L_lst, density_f_2, color='brown')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_FKM_local_num_nf_{}_L_{}_V_{}_nloc_{}.pdf'.format(nf, L, V, n_local_2))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("Local density $<n_i>$", size = 18)
plt.xlabel("Lattice site $i$", size = 18)

plt.scatter(L_lst, density_d_final, color='green', label = "d-fermions")
plt.plot(L_lst, density_d_final, color='green')

plt.scatter(L_lst, density_f_final, color='brown', label = "f-fermions")
plt.plot(L_lst, density_f_final, color='brown')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_FKM_local_num_nf_{}_L_{}_V_{}_nloc_{}.pdf'.format(nf, L, V, n_times))

plt.show()



# Plot for the local current:
    
# Lattice:
f_lattice_x = []
f_lattice_y = []
d_lattice_x = []
d_lattice_y = []

for i in range(L//2):
    
    f_lattice_x.append(i)
    f_lattice_y.append(0)
    
    d_lattice_x.append(i)
    d_lattice_y.append(1)


# Length of the arrows:
dx_hor = 1
dy_hor = 0
dx_ver = 0
dy_ver = 1

epsilon = 10**(-4)

plt.figure(figsize=(9, 7))

# Local current as arrows:
for i in range(len(j_local_f_1)):
    if (abs(j_local_f_1[i]) < epsilon):
        print("No current at this point")
        
    elif(j_local_f_1[i] > 0):
        plt.arrow(i+1, 0, -dx_hor, -dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')

    elif(j_local_f_1[i] < 0):
        plt.arrow(i, 0, dx_hor, dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')


for i in range(len(j_local_d_1)):
    if (abs(j_local_d_1[i]) < epsilon):
        print("No current at this point")
        
    elif(j_local_d_1[i] > 0):
        plt.arrow(i+1, 1, -dx_hor, -dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')

    elif(j_local_d_1[i] < 0):
        plt.arrow(i, 1, dx_hor, dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')


for i in range(len(j_local_df_1)):
    if (abs(j_local_df_1[i]) < epsilon):
        print("No current at this point")
        
    elif(j_local_df_1[i] > 0):
        plt.arrow(i, 1, -dx_ver, -dy_ver, head_length = 0.08, head_width = 0.3, color='black', length_includes_head='True')

    elif(j_local_df_1[i] < 0):
        plt.arrow(i, 0, dx_ver, dy_ver, head_length = 0.08, head_width = 0.3, color='black', length_includes_head='True')


# Lattice:
plt.scatter(f_lattice_x, f_lattice_y, s = 150, edgecolors = 'black', color='blue', label = "f-fermion")

plt.scatter(d_lattice_x, d_lattice_y, s = 150, edgecolors = 'black', color='orange', label = "d-fermion")

plt.ylim(-0.2, 1.25)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_local_current_L_{}_nf_{}_V_{}_tf_{}_Ef_{}_nloc_{}.pdf'.format(L, nf, V, t_f, epsilon_f, n_local_1))

plt.show()



plt.figure(figsize=(9, 7))

# Local current as arrows:
for i in range(len(j_local_f_2)):
    if (abs(j_local_f_2[i]) < epsilon):
        print("No current at this point")
        
    elif(j_local_f_2[i] > 0):
        plt.arrow(i+1, 0, -dx_hor, -dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')

    elif(j_local_f_2[i] < 0):
        plt.arrow(i, 0, dx_hor, dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')


for i in range(len(j_local_d_2)):
    if (abs(j_local_d_2[i]) < epsilon):
        print("No current at this point")
        
    elif(j_local_d_2[i] > 0):
        plt.arrow(i+1, 1, -dx_hor, -dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')

    elif(j_local_d_2[i] < 0):
        plt.arrow(i, 1, dx_hor, dy_hor, head_length = 0.3, head_width = 0.04, color='black', length_includes_head='True')


for i in range(len(j_local_df_2)):
    if (abs(j_local_df_2[i]) < epsilon):
        print("No current at this point")
        
    elif(j_local_df_2[i] > 0):
        plt.arrow(i, 1, -dx_ver, -dy_ver, head_length = 0.08, head_width = 0.3, color='black', length_includes_head='True')

    elif(j_local_df_2[i] < 0):
        plt.arrow(i, 0, dx_ver, dy_ver, head_length = 0.08, head_width = 0.3, color='black', length_includes_head='True')


# Lattice:
plt.scatter(f_lattice_x, f_lattice_y, s = 150, edgecolors = 'black', color='blue', label = "f-fermion")

plt.scatter(d_lattice_x, d_lattice_y, s = 150, edgecolors = 'black', color='orange', label = "d-fermion")

plt.ylim(-0.2, 1.25)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_local_current_L_{}_nf_{}_V_{}_tf_{}_Ef_{}_nloc_{}.pdf'.format(L, nf, V, t_f, epsilon_f, n_local_2))

plt.show()



#%% Calculation integrated current


print("Current offset: ", current_offset)
print("Current d offset: ", d_offset)
print("Current f offset: ", f_offset)
  
