# .txt-files for the Plots of the photocurrent

The L in the name of the txt-files corresponds to the \Tilde{L} of the linearized FKM-Hamiltonian.

First column: Time

Second column: (d-, f-, df- or integrated) Current


