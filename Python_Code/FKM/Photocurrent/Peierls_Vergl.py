import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear, Time_evolution_Lanczos_opt
from Functions import current_d, current_f, current_df, local_current
from Functions import Local_fermion_number_linear


import numpy as np
import matplotlib.pyplot as plt

import timeit

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1.
t_d = 1. # unit of energy
t_f = -0.1
V = 0.
U = 2.


# Particle density:
nf = 0.7

# Number of lattice sites:
L_20 = 20
L_24 = 24

# Number of iteration steps for the Lanczos algorithm:
k = 140 
k_t = 20

# Considered time:
dt = 0.01 # Time step
t_list = np.arange(0, 20, dt)


# Variables of the Peierls phase:
t_0 = 10.
A_0 = 1.
sigma = 1.7
omega = 4.

exp_on_off = 1 # Factor to turn the gaussian part of the Peierls on or off

#%% Current with Peierls phase

# Files for saving the data:
file_j_d_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_L1_j_d_L_{}_U_{}.txt".format(L_20, U), 'w')
file_j_f_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_L1_j_f_L_{}_U_{}.txt".format(L_20, U), 'w')
file_j_df_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_L1_j_df_L_{}_U_{}.txt".format(L_20, U), 'w')

# Files for saving the data:
file_j_d_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_L2_j_d_L_{}_U_{}.txt".format(L_24, U), 'w')
file_j_f_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_L2_j_f_L_{}_U_{}.txt".format(L_24, U), 'w')
file_j_df_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Peierls_Verg_L2_j_df_L_{}_U_{}.txt".format(L_24, U), 'w')


# Number of time steps:
n_times = len(t_list)
print("Total number of time steps: ", n_times)


# Hamiltonian for t = 0:
print("Start calculation of the ground state")
H_t0_20, n_20 = Hamiltonian_FKM_linear(L_20, t_d, t_f, epsilon_d, epsilon_f, U, V, nf)
H_t0_24, n_24 = Hamiltonian_FKM_linear(L_24, t_d, t_f, epsilon_d, epsilon_f, U, V, nf)

# Test vector:
phi_20 = np.ones(n_20)
phi_0_20 = phi_20/np.linalg.norm(phi_20)

phi_24 = np.ones(n_24)
phi_0_24 = phi_24/np.linalg.norm(phi_24)

E_20, V_matrix_20, Q_T_20 = lanczos_iter(H_t0_20, phi_0_20, k)
E_24, V_matrix_24, Q_T_24 = lanczos_iter(H_t0_24, phi_0_24, k)

# Ground state of the system in the original basis:
state_t0_20 = lin_comb_Q_T(V_matrix_20[:,0], Q_T_20)
state_t0_24 = lin_comb_Q_T(V_matrix_24[:,0], Q_T_24)

print("End calculation of the ground state")


# Peierls phase time evolution:
print("Start of time evolution")

# Pump pulse:
def Peierls_phase_pump_pulse_d(times, t_0, A_0, sigma, omega, t_d, exp_on_off):
    """
    ------------------
    times: Considered time
    t_0: Center of the pump pulse
    A_0: Amplitude of the pump pulse
    sigma: Width of the pump pulse
    omega: Frequency of the pump pulse
    t_d: Hopping amplitude of the d-electrons
    ------------------
    """
    A = A_0 * np.exp(-exp_on_off * (times - t_0)**2 / (2 * sigma**2)) * np.cos(omega * (times - t_0))
    return t_d * np.exp(1j * A)

def Peierls_phase_pump_pulse_f(times, t_0, A_0, sigma, omega, t_f, exp_on_off):
    """
    ------------------
    times: Considered time
    t_0: Center of the pump pulse
    A_0: Amplitude of the pump pulse
    sigma: Width of the pump pulse
    omega: Frequency of the pump pulse
    t_f: Hopping amplitude of the f-electrons
    ------------------
    """
    A = A_0 * np.exp(-exp_on_off * (times - t_0)**2 / (2 * sigma**2)) * np.cos(omega * (times - t_0))
    return t_f * np.exp(1j * A)


# Operators for the currents:
j_d_op_20 = current_d(t_d, L_20, nf).tocsr()
j_f_op_20 = current_f(t_f, L_20, nf).tocsr()
j_df_op_20 = current_df(V, L_20, nf).tocsr()

j_d_op_24 = current_d(t_d, L_24, nf).tocsr()
j_f_op_24 = current_f(t_f, L_24, nf).tocsr()
j_df_op_24 = current_df(V, L_24, nf).tocsr()


# List for the global current:
j_d_20 = []
j_f_20 = []
j_df_20 = []

j_d_24 = []
j_f_24 = []
j_df_24 = []


start_time_evolution = timeit.default_timer()

for i in range(n_times):
    print("Step of time evolution: ", i + 1)
    # Calculation of the Hamiltonian:
    Peierls_d = Peierls_phase_pump_pulse_d(i * dt, t_0, A_0, sigma, omega, t_d, exp_on_off)
    Peierls_f = Peierls_phase_pump_pulse_f(i * dt, t_0, A_0, sigma, omega, t_f, exp_on_off)


    H_t_20, n_20 = Hamiltonian_FKM_linear(L_20, Peierls_d, Peierls_f, epsilon_d, epsilon_f, U, V, nf)
    H_t_24, n_24 = Hamiltonian_FKM_linear(L_24, Peierls_d, Peierls_f, epsilon_d, epsilon_f, U, V, nf)
    
    H_sparse_20 = H_t_20.tocsr()
    H_sparse_24 = H_t_24.tocsr()
    
    # Lanczos time evolution:
    state_t_20 = Time_evolution_Lanczos_opt(k_t, H_sparse_20, state_t0_20, dt, n_20)
    state_t_24 = Time_evolution_Lanczos_opt(k_t, H_sparse_24, state_t0_24, dt, n_24)
    
    
    # Calculation of the expectation values:
    j_d_exp_20 = np.conj(state_t_20).dot(j_d_op_20.dot(state_t_20))
    j_d_20.append(j_d_exp_20)
    j_d_exp_24 = np.conj(state_t_24).dot(j_d_op_24.dot(state_t_24))
    j_d_24.append(j_d_exp_24)
    
    j_f_exp_20 = np.conj(state_t_20).dot(j_f_op_20.dot(state_t_20))
    j_f_20.append(j_f_exp_20)
    j_f_exp_24 = np.conj(state_t_24).dot(j_f_op_24.dot(state_t_24))
    j_f_24.append(j_f_exp_24)
    
    j_df_exp_20 = np.conj(state_t_20).dot(j_df_op_20.dot(state_t_20))
    j_df_20.append(j_df_exp_20)
    j_df_exp_24 = np.conj(state_t_24).dot(j_df_op_24.dot(state_t_24))
    j_df_24.append(j_df_exp_24)

    file_j_d_1.write('{}\t {}\n'.format(i*dt, j_d_20[i]))
    file_j_f_1.write('{}\t {}\n'.format(i*dt, j_f_20[i]))
    file_j_df_1.write('{}\t {}\n'.format(i*dt, j_df_20[i]))
        
    file_j_d_2.write('{}\t {}\n'.format(i*dt, j_d_24[i]))
    file_j_f_2.write('{}\t {}\n'.format(i*dt, j_f_24[i]))
    file_j_df_2.write('{}\t {}\n'.format(i*dt, j_df_24[i]))
    
    state_t0_20 = state_t_20
    state_t0_24 = state_t_24
    
    
stop_time_evolution = timeit.default_timer()    
runtime_time_evolution = stop_time_evolution - start_time_evolution       
        
print("Runtime of time evolution: ", runtime_time_evolution)
      
print("End of time evolution")

file_j_d_1.close()
file_j_f_1.close()
file_j_df_1.close()

file_j_d_2.close()
file_j_f_2.close()
file_j_df_2.close()


# Calculation of global current:
j_global_20 = [j_d_20[i] + j_f_20[i] + j_df_20[i] for i in range(n_times)]
j_global_24 = [j_d_24[i] + j_f_24[i] + j_df_24[i] for i in range(n_times)]

summe_j_offset_20 = 0
j_int_list_20 = []
summe_j_offset_24 = 0
j_int_list_24 = []

for i in range(n_times):
    summe_j_offset_20 += j_global_20[i]
    summe_j_offset_24 += j_global_24[i]
    
    j_int_list_20.append(summe_j_offset_20 / i)
    j_int_list_24.append(summe_j_offset_24 / i)
    
current_offset_20 = summe_j_offset_20 / n_times
current_offset_24 = summe_j_offset_24 / n_times
   


# Plots:

plt.figure(figsize=(9, 7))

plt.ylabel("Total current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list, j_global_20, color='green', label = 'L = 10', zorder = 1)
plt.plot(t_list, j_global_24, color='orange', label = 'L = 12', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_Peierls_global_current_verg_nf_{}_U_{}_V_{}_A_{}.pdf'.format(nf, U, V, A_0))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("Integrated current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list, j_int_list_20, color='green', label = 'L = 10', zorder = 1)
plt.plot(t_list, j_int_list_24, color='orange', label = 'L = 12', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_Peierls_int_current_verg_nf_{}_U_{}_V_{}_A_{}.pdf'.format(nf, U, V, A_0))

plt.show()


#%% Calculation integrated current

print("Current offset L = 10: ", current_offset_20)
print("Current offset L = 12: ", current_offset_24)
    
    