import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear, Time_evolution_Lanczos_opt
from Functions import current_d, current_f, current_df, local_current
from Functions import Local_fermion_number_linear


import numpy as np
import matplotlib.pyplot as plt

import timeit

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1.
t_d = 1. # unit of energy
t_f = -0.1
V = 4.

U_t0 = -7.5 # U for t = 0
U_t = 0. # U for t > 0


# Particle density:
nf = 0.7
 
# Number of lattice sites:
L = 20
# Number of iteration steps for the Lanczos algorithm:
k = 140 
k_t = 20

# Considered time:
dt_1 = 0.01 # Time step
dt_2 = 0.005 # Time step
t_list_1 = np.arange(0, 80, dt_1)
t_list_2 = np.arange(0, 80, dt_2)

#%% Current with Quench

# Files for saving the data:
file_j_d_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_Verg_dt1_j_d_L_{}_U_{}.txt".format(L, U_t0), 'w')
file_j_f_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_Verg_dt1_j_f_L_{}_U_{}.txt".format(L, U_t0), 'w')
file_j_df_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_Verg_dt1_j_df_L_{}_U_{}.txt".format(L, U_t0), 'w')

# Files for saving the data:
file_j_d_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_Verg_dt2_j_d_L_{}_U_{}.txt".format(L, U_t0), 'w')
file_j_f_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_Verg_dt2_j_f_L_{}_U_{}.txt".format(L, U_t0), 'w')
file_j_df_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Photocurrent/Quench_Verg_dt2_j_df_L_{}_U_{}.txt".format(L, U_t0), 'w')


# Number of time steps:
n_times_1 = len(t_list_1)
n_times_2 = len(t_list_2)

# Hamiltonian for t = 0:
print("Start calculation of the ground state")
H_t0, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U_t0, V, nf)

# Test vector:
phi = np.ones(n)
phi_0 = phi/np.linalg.norm(phi)

E, V_matrix, Q_T = lanczos_iter(H_t0, phi_0, k)

# Ground state of the system in the original basis:
state_t0_1 = lin_comb_Q_T(V_matrix[:,0], Q_T)
state_t0_2 = lin_comb_Q_T(V_matrix[:,0], Q_T)

print("End calculation of the ground state")


# Quench of U:
print("Start of time evolution")

# Operators for the currents:
j_d_op = current_d(t_d, L, nf).tocsr()
j_f_op = current_f(t_f, L, nf).tocsr()
j_df_op = current_df(V, L, nf).tocsr()


# List for the global current:
j_d_1 = []
j_f_1 = []
j_df_1 = []

j_d_2 = []
j_f_2 = []
j_df_2 = []

start_time_evolution = timeit.default_timer()

for i in range(n_times_1):
    print("Step of time evolution: ", i + 1)
    # Calculation of the Hamiltonian:
       
    H_t, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U_t, V, nf)    

    H_sparse = H_t.tocsr()
    
    # Lanczos time evolution:
    state_t = Time_evolution_Lanczos_opt(k_t, H_sparse, state_t0_1, dt_1, n)

    
    # Calculation of the expectation values:
    j_d_exp = np.conj(state_t).dot(j_d_op.dot(state_t))
    j_d_1.append(j_d_exp)
    
    j_f_exp = np.conj(state_t).dot(j_f_op.dot(state_t))
    j_f_1.append(j_f_exp)
    
    j_df_exp = np.conj(state_t).dot(j_df_op.dot(state_t))
    j_df_1.append(j_df_exp)
    
    file_j_d_1.write('{}\t {}\n'.format(i*dt_1, j_d_1[i]))
    file_j_f_1.write('{}\t {}\n'.format(i*dt_1, j_f_1[i]))
    file_j_df_1.write('{}\t {}\n'.format(i*dt_1, j_df_1[i]))
    
    state_t0_1 = state_t
    
    

for i in range(n_times_2):
    print("Step of time evolution: ", i + 1)
    # Calculation of the Hamiltonian:
       
    H_t, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U_t, V, nf)    

    H_sparse = H_t.tocsr()
    
    # Lanczos time evolution:
    state_t = Time_evolution_Lanczos_opt(k_t, H_sparse, state_t0_2, dt_2, n)

    
    # Calculation of the expectation values:
    j_d_exp = np.conj(state_t).dot(j_d_op.dot(state_t))
    j_d_2.append(j_d_exp)
    
    j_f_exp = np.conj(state_t).dot(j_f_op.dot(state_t))
    j_f_2.append(j_f_exp)

    j_df_exp = np.conj(state_t).dot(j_df_op.dot(state_t))
    j_df_2.append(j_df_exp)
    
    file_j_d_2.write('{}\t {}\n'.format(i*dt_2, j_d_2[i]))
    file_j_f_2.write('{}\t {}\n'.format(i*dt_2, j_f_2[i]))
    file_j_df_2.write('{}\t {}\n'.format(i*dt_2, j_df_2[i]))
    
    state_t0_2 = state_t
    
stop_time_evolution = timeit.default_timer()    
runtime_time_evolution = stop_time_evolution - start_time_evolution       
        
print("Runtime of time evolution: ", runtime_time_evolution)

print("End of time evolution")

file_j_d_1.close()
file_j_f_1.close()
file_j_df_1.close()

file_j_d_2.close()
file_j_f_2.close()
file_j_df_2.close()


# Calculation of global current:
j_global_1 = [j_d_1[i] + j_f_1[i] + j_df_1[i] for i in range(n_times_1)]
j_global_2 = [j_d_2[i] + j_f_2[i] + j_df_2[i] for i in range(n_times_2)]
   
summe_j_offset_1 = 0
j_int_list_1 = []

summe_j_offset_2 = 0
j_int_list_2 = []

for i in range(n_times_1):
    summe_j_offset_1 += j_global_1[i]
    
    j_int_list_1.append(summe_j_offset_1 / i)
    
for i in range(n_times_2):
    summe_j_offset_2 += j_global_2[i]
    
    j_int_list_2.append(summe_j_offset_2 / i)
    

current_offset_1 = summe_j_offset_1 / n_times_1
current_offset_2 = summe_j_offset_2 / n_times_2



# Plots:


plt.figure(figsize=(9, 7))

plt.ylabel("Total current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list_1, j_global_1, color='green', label = '$dt = 0.01$', zorder = 1)
plt.plot(t_list_2, j_global_2, color='orange', label = '$dt = 0.005$', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_global_current_verg_dt_nf_{}_V_{}_tf_{}_Ef_{}.pdf'.format(nf, V, t_f, epsilon_f))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("Integrated current", size = 18)
plt.xlabel("Time $t$", size = 18)

plt.plot(t_list_1, j_int_list_1, color='green', label = '$dt = 0.01$', zorder = 1)
plt.plot(t_list_2, j_int_list_2, color='orange', label = '$dt = 0.005$', zorder = 1)

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$V = {}$'.format(V))

plt.savefig(my_path + '_Quench_int_current_verg_dt_nf_{}_V_{}_tf_{}_Ef_{}.pdf'.format(nf, V, t_f, epsilon_f))

plt.show()


#%% Calculation integrated current


print("Current offset dt = 0.05: ", current_offset_1)
print("Current offset dt = 0.01: ", current_offset_2)
