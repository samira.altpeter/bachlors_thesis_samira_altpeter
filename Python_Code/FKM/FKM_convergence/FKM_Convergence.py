import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear

import numpy as np
import matplotlib.pyplot as plt

from quspin.tools.lanczos import lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1. 
t_d = 1. # unit of energy
t_f = -0.1
U = 25.
V = 1.


nf = 0.7 # Particle density

# Number of lattice sites:
L1 = 16
L3 = 20
L5 = 24

# Number of iteration steps:
k_list = np.arange(1, 181, 1, dtype = int)

#%% Convergence of the Lanczos algorithm for a large number of lattice sites

file_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Convergence/Convergence_FKM_L_{}.txt".format(L1), 'w')
file_3 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Convergence/Convergence_FKM_L_{}.txt".format(L3), 'w')
file_5 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_Convergence/Convergence_FKM_L_{}.txt".format(L5), 'w')


# Hamiltonians and dimensions:
H1, n1 = Hamiltonian_FKM_linear(L1, t_d, t_f, epsilon_d, epsilon_f, U, V, nf)
H3, n3 = Hamiltonian_FKM_linear(L3, t_d, t_f, epsilon_d, epsilon_f, U, V, nf)
H5, n5 = Hamiltonian_FKM_linear(L5, t_d, t_f, epsilon_d, epsilon_f, U, V, nf)


E_ed = np.linalg.eigvalsh(H1.toarray())
E_0_ed = E_ed[0]

# Test vectors:
phi1 = np.ones(n1)
phi_0_1 = phi1/np.linalg.norm(phi1)

phi3 = np.ones(n3)
phi_0_3 = phi3/np.linalg.norm(phi3)

phi5 = np.ones(n5)
phi_0_5 = phi5/np.linalg.norm(phi5)


# Lists for the energies:
E_list1 = []
E_list3 = []
E_list5 = []

for i in range(len(k_list)):
    print("Lanczos iteration step ", i + 1)

    E1, V_matrix1, Q_T1 = lanczos_iter(H1, phi_0_1, k_list[i])
    E_list1.append(E1[0])
    
    E3, V_matrix3, Q_T3 = lanczos_iter(H3, phi_0_3, k_list[i])
    E_list3.append(E3[0])
    
    E5, V_matrix5, Q_T5 = lanczos_iter(H5, phi_0_5, k_list[i])
    E_list5.append(E5[0])
    
    file_1.write('{}\t {}\n'.format(k_list[i], E_list1[i]))
    file_3.write('{}\t {}\n'.format(k_list[i], E_list3[i]))
    file_5.write('{}\t {}\n'.format(k_list[i], E_list5[i]))


file_1.close()
file_3.close()
file_5.close()


# Ground state energies:
E0_1 = E_list1[len(k_list)-1]        
E0_3 = E_list3[len(k_list)-1]      
E0_5 = E_list5[len(k_list)-1]    



# Plot:

plt.figure(figsize=(9, 7))

plt.ylabel("Energy difference $E_i - E_0$", size = 18)
plt.xlabel("Iteration step $k$", size = 18)

plt.scatter(k_list, E_list1 - E0_1,color='blue', label = "$L={}$".format(L1//2), marker='o')
plt.scatter(k_list, E_list3 - E0_3,color='green', label = "$L={}$".format(L3//2), marker='*')
plt.scatter(k_list, E_list5 - E0_5,color='orange', label = "$L={}$".format(L5//2), marker='v')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_Convergence_n_{}_U_{}.pdf'.format(nf, U))

plt.show()


print("Lanczos end ", E0_1)
print("Lanczos k = 100", E_list1[101])
print("ED ", E_0_ed)
