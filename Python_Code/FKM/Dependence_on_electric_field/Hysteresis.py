import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_electric, Polarization_FKM_linear, Charge_location_Polar_linear

import numpy as np
import matplotlib.pyplot as plt

from scipy import optimize

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = 1.
t_d = 1. # unit of energy
t_f = -0.1 
U = 2.
V = 0.

# Electric field:
E_pos = np.arange(0.1, 4.6, 0.1)
E_neg = np.arange(-4.5, 0, 0.1)


# Particle density:
nf = 0.7

# Number of lattice sites:
L_1 = 16
L_2 = 20
L_3 = 24

k = 140 # Number of Lanczos iteration steps

#%% Hysteresis


# Files for saving the data:
file_polar_pos_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_pos_L_{}_U_{}.txt".format(L_1, U), 'w')
file_polar_pos_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_pos_L_{}_U_{}.txt".format(L_2, U), 'w')
file_polar_pos_3 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_pos_L_{}_U_{}.txt".format(L_3, U), 'w')

file_polar_neg_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_neg_L_{}_U_{}.txt".format(L_1, U), 'w')
file_polar_neg_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_neg_L_{}_U_{}.txt".format(L_2, U), 'w')
file_polar_neg_3 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_neg_L_{}_U_{}.txt".format(L_3, U), 'w')

file_ch_pos_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_pos_L_{}_U_{}.txt".format(L_1, U), 'w')
file_ch_pos_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_pos_L_{}_U_{}.txt".format(L_2, U), 'w')
file_ch_pos_3 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_pos_L_{}_U_{}.txt".format(L_3, U), 'w')

file_ch_neg_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_neg_L_{}_U_{}.txt".format(L_1, U), 'w')
file_ch_neg_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_neg_L_{}_U_{}.txt".format(L_2, U), 'w')
file_ch_neg_3 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_neg_L_{}_U_{}.txt".format(L_3, U), 'w')



polar_pos_1 = []
polar_neg_1 = []
ch_pos_1 = []
ch_neg_1 = []

polar_pos_2 = []
polar_neg_2 = []
ch_pos_2 = []
ch_neg_2 = []

polar_pos_3 = []
polar_neg_3 = []
ch_pos_3 = []
ch_neg_3 = []

print("Start calculation of positive electric field")
for i in range(len(E_pos)):
    print("E_step {} von {}".format(i + 1, len(E_pos)))

    # For L_1:

    # Operators:
    P_1 = Polarization_FKM_linear(L_1, nf)
    charge_location_op_1 = Charge_location_Polar_linear(L_1, nf)

    # Hamiltonian:
    H_1, n_1 = Hamiltonian_FKM_electric(L_1, t_d, t_f, epsilon_d, epsilon_f, U, V, E_pos[i], nf)

    # Test vector:
    phi_1 = np.ones(n_1)
    phi_0_1 = phi_1/np.linalg.norm(phi_1)
    
    E_1, V_matrix_1, Q_T_1 = lanczos_iter(H_1, phi_0_1, k)
        
    # Ground state of the system in the original basis:
    state_1 = lin_comb_Q_T(V_matrix_1[:,0], Q_T_1)
        
    # Polarization:
    polar_pos_1.append(np.conj(state_1).dot(P_1.dot(state_1)))
    ch_pos_1.append(np.conj(state_1).dot(charge_location_op_1.dot(state_1)))
    
    file_polar_pos_1.write('{}\t {}\n'.format(E_pos[i], np.conj(state_1).dot(P_1.dot(state_1))))
    file_ch_pos_1.write('{}\t {}\n'.format(E_pos[i], np.conj(state_1).dot(charge_location_op_1.dot(state_1))))

    
    
    # For L_2:

    # Operators:
    P_2 = Polarization_FKM_linear(L_2, nf)
    charge_location_op_2 = Charge_location_Polar_linear(L_2, nf)

    # Hamiltonian:
    H_2, n_2 = Hamiltonian_FKM_electric(L_2, t_d, t_f, epsilon_d, epsilon_f, U, V, E_pos[i], nf)

    # Test vector:
    phi_2 = np.ones(n_2)
    phi_0_2 = phi_2/np.linalg.norm(phi_2)
    
    E_2, V_matrix_2, Q_T_2 = lanczos_iter(H_2, phi_0_2, k)
        
    # Ground state of the system in the original basis:
    state_2 = lin_comb_Q_T(V_matrix_2[:,0], Q_T_2)
        
    # Polarization:
    polar_pos_2.append(np.conj(state_2).dot(P_2.dot(state_2)))
    ch_pos_2.append(np.conj(state_2).dot(charge_location_op_2.dot(state_2)))
    
    file_polar_pos_2.write('{}\t {}\n'.format(E_pos[i], np.conj(state_2).dot(P_2.dot(state_2))))
    file_ch_pos_2.write('{}\t {}\n'.format(E_pos[i], np.conj(state_2).dot(charge_location_op_2.dot(state_2))))

    
    
    # For L_3:

    # Operators:
    P_3 = Polarization_FKM_linear(L_3, nf)
    charge_location_op_3 = Charge_location_Polar_linear(L_3, nf)

    # Hamiltonian:
    H_3, n_3 = Hamiltonian_FKM_electric(L_3, t_d, t_f, epsilon_d, epsilon_f, U, V, E_pos[i], nf)

    # Test vector:
    phi_3 = np.ones(n_3)
    phi_0_3 = phi_3/np.linalg.norm(phi_3)
    
    E_3, V_matrix_3, Q_T_3 = lanczos_iter(H_3, phi_0_3, k)
        
    # Ground state of the system in the original basis:
    state_3 = lin_comb_Q_T(V_matrix_3[:,0], Q_T_3)
        
    # Polarization:
    polar_pos_3.append(np.conj(state_3).dot(P_3.dot(state_3)))
    ch_pos_3.append(np.conj(state_3).dot(charge_location_op_3.dot(state_3)))
    
    file_polar_pos_3.write('{}\t {}\n'.format(E_pos[i], np.conj(state_3).dot(P_3.dot(state_3))))
    file_ch_pos_3.write('{}\t {}\n'.format(E_pos[i], np.conj(state_3).dot(charge_location_op_3.dot(state_3))))

    
    print("Polarization saved")


print("Start calculation of negative electric field")
for i in range(len(E_neg)):
    print("E_step {} von {}".format(i + 1, len(E_neg)))

    # For L_1:

    # Operators:
    P_1 = Polarization_FKM_linear(L_1, nf)
    charge_location_op_1 = Charge_location_Polar_linear(L_1, nf)

    # Hamiltonian:
    H_1, n_1 = Hamiltonian_FKM_electric(L_1, t_d, t_f, epsilon_d, epsilon_f, U, V, E_neg[i], nf)

    # Test vector:
    phi_1 = np.ones(n_1)
    phi_0_1 = phi_1/np.linalg.norm(phi_1)
    
    E_1, V_matrix_1, Q_T_1 = lanczos_iter(H_1, phi_0_1, k)
        
    # Ground state of the system in the original basis:
    state_1 = lin_comb_Q_T(V_matrix_1[:,0], Q_T_1)
        
    # Polarization:
    polar_neg_1.append(np.conj(state_1).dot(P_1.dot(state_1)))
    ch_neg_1.append(np.conj(state_1).dot(charge_location_op_1.dot(state_1)))
    
    file_polar_neg_1.write('{}\t {}\n'.format(E_neg[i], np.conj(state_1).dot(P_1.dot(state_1))))
    file_ch_neg_1.write('{}\t {}\n'.format(E_neg[i], np.conj(state_1).dot(charge_location_op_1.dot(state_1))))

    
    # For L_2:

    # Operators:
    P_2 = Polarization_FKM_linear(L_2, nf)
    charge_location_op_2 = Charge_location_Polar_linear(L_2, nf)

    # Hamiltonian:
    H_2, n_2 = Hamiltonian_FKM_electric(L_2, t_d, t_f, epsilon_d, epsilon_f, U, V, E_neg[i], nf)

    # Test vector:
    phi_2 = np.ones(n_2)
    phi_0_2 = phi_2/np.linalg.norm(phi_2)
    
    E_2, V_matrix_2, Q_T_2 = lanczos_iter(H_2, phi_0_2, k)
        
    # Ground state of the system in the original basis:
    state_2 = lin_comb_Q_T(V_matrix_2[:,0], Q_T_2)
        
    # Polarization:
    polar_neg_2.append(np.conj(state_2).dot(P_2.dot(state_2)))
    ch_neg_2.append(np.conj(state_2).dot(charge_location_op_2.dot(state_2)))
    
    file_polar_neg_2.write('{}\t {}\n'.format(E_neg[i], np.conj(state_2).dot(P_2.dot(state_2))))
    file_ch_neg_2.write('{}\t {}\n'.format(E_neg[i], np.conj(state_2).dot(charge_location_op_2.dot(state_2))))
    
    
    # For L_3:

    # Operators:
    P_3 = Polarization_FKM_linear(L_3, nf)
    charge_location_op_3 = Charge_location_Polar_linear(L_3, nf)

    # Hamiltonian:
    H_3, n_3 = Hamiltonian_FKM_electric(L_3, t_d, t_f, epsilon_d, epsilon_f, U, V, E_neg[i], nf)

    # Test vector:
    phi_3 = np.ones(n_3)
    phi_0_3 = phi_3/np.linalg.norm(phi_3)
    
    E_3, V_matrix_3, Q_T_3 = lanczos_iter(H_3, phi_0_3, k)
        
    # Ground state of the system in the original basis:
    state_3 = lin_comb_Q_T(V_matrix_3[:,0], Q_T_3)
        
    # Polarization:
    polar_neg_3.append(np.conj(state_3).dot(P_3.dot(state_3)))
    ch_neg_3.append(np.conj(state_3).dot(charge_location_op_3.dot(state_3)))
    
    file_polar_neg_3.write('{}\t {}\n'.format(E_neg[i], np.conj(state_3).dot(P_3.dot(state_3))))
    file_ch_neg_3.write('{}\t {}\n'.format(E_neg[i], np.conj(state_3).dot(charge_location_op_3.dot(state_3))))

    
    print("Polarization saved")


file_polar_pos_1.close()
file_polar_pos_2.close()
file_polar_pos_3.close()

file_polar_neg_1.close()
file_polar_neg_2.close()
file_polar_neg_3.close()

file_ch_pos_1.close()
file_ch_pos_2.close()
file_ch_pos_3.close()

file_ch_neg_1.close()
file_ch_neg_2.close()
file_ch_neg_3.close()



# Plots:
    
plt.figure(figsize=(9, 7))

plt.ylabel("Polarization $P_1$", size = 18)
plt.xlabel("Electric field", size = 18)

plt.scatter(E_pos, polar_pos_1, color='green', label = '$L = {}$'.format(L_1//2))
plt.plot(E_pos, polar_pos_1, color='green')
plt.scatter(E_neg, polar_neg_1, color='green')
plt.plot(E_neg, polar_neg_1, color='green')

plt.scatter(E_pos, polar_pos_2, color='blue', label = '$L = {}$'.format(L_2//2))
plt.plot(E_pos, polar_pos_2, color='blue')
plt.scatter(E_neg, polar_neg_2, color='blue')
plt.plot(E_neg, polar_neg_2, color='blue')

plt.scatter(E_pos, polar_pos_3, color='orange', label = '$L = {}$'.format(L_3//2))
plt.plot(E_pos, polar_pos_3, color='orange')
plt.scatter(E_neg, polar_neg_3, color='orange')
plt.plot(E_neg, polar_neg_3, color='orange')


plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_FKM_Hysteresis_Polarization_nf_{}_V_{}_U_{}_tf_{}_Ef_{}.pdf'.format(nf, V, U, t_f, epsilon_f))

plt.show() 



plt.figure(figsize=(9, 7))

plt.ylabel("Polarization $P_2$", size = 18)
plt.xlabel("Electric field", size = 18)

plt.scatter(E_pos, ch_pos_1, color='green', label = '$L = {}$'.format(L_1//2))
plt.plot(E_pos, ch_pos_1, color='green')
plt.scatter(E_neg, ch_neg_1, color='green')
plt.plot(E_neg, ch_neg_1, color='green')

plt.scatter(E_pos, ch_pos_2, color='blue', label = '$L = {}$'.format(L_2//2))
plt.plot(E_pos, ch_pos_2, color='blue')
plt.scatter(E_neg, ch_neg_2, color='blue')
plt.plot(E_neg, ch_neg_2, color='blue')

plt.scatter(E_pos, ch_pos_3, color='orange', label = '$L = {}$'.format(L_3//2))
plt.plot(E_pos, ch_pos_3, color='orange')
plt.scatter(E_neg, ch_neg_3, color='orange')
plt.plot(E_neg, ch_neg_3, color='orange')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_FKM_Hysteresis_Charge_loc_nf_{}_V_{}_U_{}_tf_{}_Ef_{}.pdf'.format(nf, V, U, t_f, epsilon_f))

plt.show() 
    
#%%

# Files for saving the data:
file_polar_pos_1_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_pos_L_{}_U_{}_zoom.txt".format(L_1, U), 'w')
file_polar_pos_2_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_pos_L_{}_U_{}_zoom.txt".format(L_2, U), 'w')
file_polar_pos_3_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_pos_L_{}_U_{}_zoom.txt".format(L_3, U), 'w')

file_polar_neg_1_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_neg_L_{}_U_{}_zoom.txt".format(L_1, U), 'w')
file_polar_neg_2_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_neg_L_{}_U_{}_zoom.txt".format(L_2, U), 'w')
file_polar_neg_3_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P1_neg_L_{}_U_{}_zoom.txt".format(L_3, U), 'w')

file_ch_pos_1_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_pos_L_{}_U_{}_zoom.txt".format(L_1, U), 'w')
file_ch_pos_2_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_pos_L_{}_U_{}_zoom.txt".format(L_2, U), 'w')
file_ch_pos_3_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_pos_L_{}_U_{}_zoom.txt".format(L_3, U), 'w')

file_ch_neg_1_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_neg_L_{}_U_{}_zoom.txt".format(L_1, U), 'w')
file_ch_neg_2_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_neg_L_{}_U_{}_zoom.txt".format(L_2, U), 'w')
file_ch_neg_3_zoom = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/Hysteresis/P2_neg_L_{}_U_{}_zoom.txt".format(L_3, U), 'w')




# Model for linear fitting: 
def model(x, a, b):
    return a*x + b
ndf = 2

epsilon = 10**(-8)

# Electric field:
E_pos = np.arange(0.001, 0.101, 0.001)
E_neg = np.arange(-0.1, 0, 0.001)

# Electric field for which the linear fitting is done:
E_pos_zoom = np.arange(0.001, 0.013, 0.001)
E_neg_zoom = np.arange(-0.012, 0, 0.001)


polar_pos_1 = []
polar_neg_1 = []
ch_pos_1 = []
ch_neg_1 = []

ch_pos_1_zoom = []
ch_neg_1_zoom = []

polar_pos_2 = []
polar_neg_2 = []
ch_pos_2 = []
ch_neg_2 = []

ch_pos_2_zoom = []
ch_neg_2_zoom = []

polar_pos_3 = []
polar_neg_3 = []
ch_pos_3 = []
ch_neg_3 = []

ch_pos_3_zoom = []
ch_neg_3_zoom = []


print("Start calculation of positive electric field")
for i in range(len(E_pos)):
    print("E_step {} von {}".format(i + 1, len(E_pos)))

    # For L_1:

    # Operators:
    P_1 = Polarization_FKM_linear(L_1, nf)
    charge_location_op_1 = Charge_location_Polar_linear(L_1, nf)

    # Hamiltonian:
    H_1, n_1 = Hamiltonian_FKM_electric(L_1, t_d, t_f, epsilon_d, epsilon_f, U, V, E_pos[i], nf)

    # Test vector:
    phi_1 = np.ones(n_1)
    phi_0_1 = phi_1/np.linalg.norm(phi_1)
    
    E_1, V_matrix_1, Q_T_1 = lanczos_iter(H_1, phi_0_1, k)
        
    # Ground state of the system in the original basis:
    state_1 = lin_comb_Q_T(V_matrix_1[:,0], Q_T_1)
        
    # Polarization:
    polar_pos_1.append(np.conj(state_1).dot(P_1.dot(state_1)))
    ch_pos_1.append(np.conj(state_1).dot(charge_location_op_1.dot(state_1)))
    
    file_polar_pos_1_zoom.write('{}\t {}\n'.format(E_pos[i], np.conj(state_1).dot(P_1.dot(state_1))))
    file_ch_pos_1_zoom.write('{}\t {}\n'.format(E_pos[i], np.conj(state_1).dot(charge_location_op_1.dot(state_1))))

    
    # For L_2:

    # Operators:
    P_2 = Polarization_FKM_linear(L_2, nf)
    charge_location_op_2 = Charge_location_Polar_linear(L_2, nf)

    # Hamiltonian:
    H_2, n_2 = Hamiltonian_FKM_electric(L_2, t_d, t_f, epsilon_d, epsilon_f, U, V, E_pos[i], nf)

    # Test vector:
    phi_2 = np.ones(n_2)
    phi_0_2 = phi_2/np.linalg.norm(phi_2)
    
    E_2, V_matrix_2, Q_T_2 = lanczos_iter(H_2, phi_0_2, k)
        
    # Ground state of the system in the original basis:
    state_2 = lin_comb_Q_T(V_matrix_2[:,0], Q_T_2)
        
    # Polarization:
    polar_pos_2.append(np.conj(state_2).dot(P_2.dot(state_2)))
    ch_pos_2.append(np.conj(state_2).dot(charge_location_op_2.dot(state_2)))
    
    file_polar_pos_2_zoom.write('{}\t {}\n'.format(E_pos[i], np.conj(state_2).dot(P_2.dot(state_2))))
    file_ch_pos_2_zoom.write('{}\t {}\n'.format(E_pos[i], np.conj(state_2).dot(charge_location_op_2.dot(state_2))))

    
    # For L_3:

    # Operators:
    P_3 = Polarization_FKM_linear(L_3, nf)
    charge_location_op_3 = Charge_location_Polar_linear(L_3, nf)

    # Hamiltonian:
    H_3, n_3 = Hamiltonian_FKM_electric(L_3, t_d, t_f, epsilon_d, epsilon_f, U, V, E_pos[i], nf)

    # Test vector:
    phi_3 = np.ones(n_3)
    phi_0_3 = phi_3/np.linalg.norm(phi_3)
    
    E_3, V_matrix_3, Q_T_3 = lanczos_iter(H_3, phi_0_3, k)
        
    # Ground state of the system in the original basis:
    state_3 = lin_comb_Q_T(V_matrix_3[:,0], Q_T_3)
        
    # Polarization:
    polar_pos_3.append(np.conj(state_3).dot(P_3.dot(state_3)))
    ch_pos_3.append(np.conj(state_3).dot(charge_location_op_3.dot(state_3)))
    
    file_polar_pos_3_zoom.write('{}\t {}\n'.format(E_pos[i], np.conj(state_3).dot(P_3.dot(state_3))))
    file_ch_pos_3_zoom.write('{}\t {}\n'.format(E_pos[i], np.conj(state_3).dot(charge_location_op_3.dot(state_3))))

    
    for j in range(len(E_pos_zoom)):
        
        if abs(E_pos[i] - E_pos_zoom[j]) <= epsilon:
            ch_pos_1_zoom.append(np.conj(state_1).dot(charge_location_op_1.dot(state_1)))
            ch_pos_2_zoom.append(np.conj(state_2).dot(charge_location_op_2.dot(state_2)))
            ch_pos_3_zoom.append(np.conj(state_3).dot(charge_location_op_3.dot(state_3)))
    
    
    print("Polarization saved")


print("Start calculation of negative electric field")
for i in range(len(E_neg)):
    print("E_step {} von {}".format(i + 1, len(E_neg)))

    # For L_1:

    # Operators:
    P_1 = Polarization_FKM_linear(L_1, nf)
    charge_location_op_1 = Charge_location_Polar_linear(L_1, nf)

    # Hamiltonian:
    H_1, n_1 = Hamiltonian_FKM_electric(L_1, t_d, t_f, epsilon_d, epsilon_f, U, V, E_neg[i], nf)

    # Test vector:
    phi_1 = np.ones(n_1)
    phi_0_1 = phi_1/np.linalg.norm(phi_1)
    
    E_1, V_matrix_1, Q_T_1 = lanczos_iter(H_1, phi_0_1, k)
        
    # Ground state of the system in the original basis:
    state_1 = lin_comb_Q_T(V_matrix_1[:,0], Q_T_1)
        
    # Polarization:
    polar_neg_1.append(np.conj(state_1).dot(P_1.dot(state_1)))
    ch_neg_1.append(np.conj(state_1).dot(charge_location_op_1.dot(state_1)))
    
    file_polar_neg_1_zoom.write('{}\t {}\n'.format(E_neg[i], np.conj(state_1).dot(P_1.dot(state_1))))
    file_ch_neg_1_zoom.write('{}\t {}\n'.format(E_neg[i], np.conj(state_1).dot(charge_location_op_1.dot(state_1))))

    
    # For L_2:

    # Operators:
    P_2 = Polarization_FKM_linear(L_2, nf)
    charge_location_op_2 = Charge_location_Polar_linear(L_2, nf)

    # Hamiltonian:
    H_2, n_2 = Hamiltonian_FKM_electric(L_2, t_d, t_f, epsilon_d, epsilon_f, U, V, E_neg[i], nf)

    # Test vector:
    phi_2 = np.ones(n_2)
    phi_0_2 = phi_2/np.linalg.norm(phi_2)
    
    E_2, V_matrix_2, Q_T_2 = lanczos_iter(H_2, phi_0_2, k)
        
    # Ground state of the system in the original basis:
    state_2 = lin_comb_Q_T(V_matrix_2[:,0], Q_T_2)
        
    # Polarization:
    polar_neg_2.append(np.conj(state_2).dot(P_2.dot(state_2)))
    ch_neg_2.append(np.conj(state_2).dot(charge_location_op_2.dot(state_2)))
    
    file_polar_neg_2_zoom.write('{}\t {}\n'.format(E_neg[i], np.conj(state_2).dot(P_2.dot(state_2))))
    file_ch_neg_2_zoom.write('{}\t {}\n'.format(E_neg[i], np.conj(state_2).dot(charge_location_op_2.dot(state_2))))
    
    
    # For L_3:

    # Operators:
    P_3 = Polarization_FKM_linear(L_3, nf)
    charge_location_op_3 = Charge_location_Polar_linear(L_3, nf)

    # Hamiltonian:
    H_3, n_3 = Hamiltonian_FKM_electric(L_3, t_d, t_f, epsilon_d, epsilon_f, U, V, E_neg[i], nf)

    # Test vector:
    phi_3 = np.ones(n_3)
    phi_0_3 = phi_3/np.linalg.norm(phi_3)
    
    E_3, V_matrix_3, Q_T_3 = lanczos_iter(H_3, phi_0_3, k)
        
    # Ground state of the system in the original basis:
    state_3 = lin_comb_Q_T(V_matrix_3[:,0], Q_T_3)
        
    # Polarization:
    polar_neg_3.append(np.conj(state_3).dot(P_3.dot(state_3)))
    ch_neg_3.append(np.conj(state_3).dot(charge_location_op_3.dot(state_3)))
    
    file_polar_neg_3_zoom.write('{}\t {}\n'.format(E_neg[i], np.conj(state_3).dot(P_3.dot(state_3))))
    file_ch_neg_3_zoom.write('{}\t {}\n'.format(E_neg[i], np.conj(state_3).dot(charge_location_op_3.dot(state_3))))

    
    for j in range(len(E_neg_zoom)):
        
        if abs(E_neg[i] - E_neg_zoom[j]) <= epsilon:
            ch_neg_1_zoom.append(np.conj(state_1).dot(charge_location_op_1.dot(state_1)))
            ch_neg_2_zoom.append(np.conj(state_2).dot(charge_location_op_2.dot(state_2)))
            ch_neg_3_zoom.append(np.conj(state_3).dot(charge_location_op_3.dot(state_3)))
        
    
    print("Polarization saved")


file_polar_pos_1_zoom.close()
file_polar_pos_2_zoom.close()
file_polar_pos_3_zoom.close()

file_polar_neg_1_zoom.close()
file_polar_neg_2_zoom.close()
file_polar_neg_3_zoom.close()

file_ch_pos_1_zoom.close()
file_ch_pos_2_zoom.close()
file_ch_pos_3_zoom.close()

file_ch_neg_1_zoom.close()
file_ch_neg_2_zoom.close()
file_ch_neg_3_zoom.close()



ans_pos_1, cov_pos_1 = optimize.curve_fit(model, E_pos_zoom, ch_pos_1_zoom)
ans_pos_2, cov_pos_2 = optimize.curve_fit(model, E_pos_zoom, ch_pos_2_zoom)
ans_pos_3, cov_pos_3 = optimize.curve_fit(model, E_pos_zoom, ch_pos_3_zoom)

ans_neg_1, cov_neg_1 = optimize.curve_fit(model, E_neg_zoom, ch_neg_1_zoom)
ans_neg_2, cov_neg_2 = optimize.curve_fit(model, E_neg_zoom, ch_neg_2_zoom)
ans_neg_3, cov_neg_3 = optimize.curve_fit(model, E_neg_zoom, ch_neg_3_zoom)


E_pos_fit = np.arange(-0.002, 0.025, 0.001)
E_neg_fit = np.arange(-0.025, 0.003, 0.001)



plt.figure(figsize=(9, 7))

plt.ylabel("Polarization $P_2$", size = 18)
plt.xlabel("Electric field", size = 18)

plt.scatter(E_pos, ch_pos_1, color='green', label = '$L = {}$'.format(L_1//2))
plt.plot(E_pos, ch_pos_1, color='green')
plt.scatter(E_neg, ch_neg_1, color='green')
plt.plot(E_neg, ch_neg_1, color='green')
plt.plot(E_pos_fit, model(E_pos_fit, ans_pos_1[0], ans_pos_1[1]), color='red')
plt.plot(E_neg_fit, model(E_neg_fit, ans_neg_1[0], ans_neg_1[1]), color='red')

plt.scatter(E_pos, ch_pos_2, color='blue', label = '$L = {}$'.format(L_2//2))
plt.plot(E_pos, ch_pos_2, color='blue')
plt.scatter(E_neg, ch_neg_2, color='blue')
plt.plot(E_neg, ch_neg_2, color='blue')
plt.plot(E_pos_fit, model(E_pos_fit, ans_pos_2[0], ans_pos_2[1]), color='red')
plt.plot(E_neg_fit, model(E_neg_fit, ans_neg_2[0], ans_neg_2[1]), color='red')

plt.scatter(E_pos, ch_pos_3, color='orange', label = '$L = {}$'.format(L_3//2))
plt.plot(E_pos, ch_pos_3, color='orange')
plt.scatter(E_neg, ch_neg_3, color='orange')
plt.plot(E_neg, ch_neg_3, color='orange')
plt.plot(E_pos_fit, model(E_pos_fit, ans_pos_3[0], ans_pos_3[1]), color='red', label = 'Linear fits')
plt.plot(E_neg_fit, model(E_neg_fit, ans_neg_3[0], ans_neg_3[1]), color='red')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(title = '$U = {}$, $V = {}$'.format(U, V))

plt.savefig(my_path + '_FKM_Hysteresis_Charge_loc_nf_{}_V_{}_U_{}_tf_{}_Ef_{}_zoom.pdf'.format(nf, V, U, t_f, epsilon_f))

plt.show() 


#%%

print("positive ans 1 ", ans_pos_1)
print("negative ans 1 ", ans_neg_1)

print("positive ans 2 ", ans_pos_2)
print("negative ans 2 ", ans_neg_2)

print("positive ans 3 ", ans_pos_3)
print("negative ans 3 ", ans_neg_3)
    