import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Hamiltonian_FKM_linear
from Functions import Fermion_d_number_linear, Fermion_f_number_linear

import numpy as np
import matplotlib.pyplot as plt

from quspin.tools.lanczos import lin_comb_Q_T, lanczos_iter

import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Parameters of the FKM:
epsilon_d = 0.5
epsilon_f = np.arange(0, 2.5, 0.05)
t_d = 1. # unit of energy
t_f = -0.1

U1 = 0.5
U2 = 1.
U3 = 5.
U4 = 10.
U5 = 25.
V = 1.

# Particle density:
nf = 0.7

L = 20 # Number of lattice sites

k = 140 # Number of Lanczos iteration steps

#%% Observables for different epsilon_f

# Files for saving the data:
file_num_d_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_d_U_{}.txt".format(U1), 'w')
file_num_d_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_d_U_{}.txt".format(U2), 'w')
file_num_d_3 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_d_U_{}.txt".format(U3), 'w')
file_num_d_4 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_d_U_{}.txt".format(U4), 'w')
file_num_d_5 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_d_U_{}.txt".format(U5), 'w')

file_num_f_1 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_f_U_{}.txt".format(U1), 'w')
file_num_f_2 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_f_U_{}.txt".format(U2), 'w')
file_num_f_3 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_f_U_{}.txt".format(U3), 'w')
file_num_f_4 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_f_U_{}.txt".format(U4), 'w')
file_num_f_5 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/FKM/FKM_diff_E_f/num_f_U_{}.txt".format(U5), 'w')


# Operators:
num_d_op = Fermion_d_number_linear(L, nf)
num_f_op = Fermion_f_number_linear(L, nf)


# Lists for the expectation values:
num_d_1 = []
num_d_2 = []
num_d_3 = []
num_d_4 = []
num_d_5 = []

num_f_1 = []
num_f_2 = []
num_f_3 = []
num_f_4 = []
num_f_5 = []


for i in range(len(epsilon_f)):
    print("Number of calculatin step: ", i + 1)
    
    #Hamiltonians for different U:
    H_1, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f[i], U1, V, nf)
    H_2, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f[i], U2, V, nf)
    H_3, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f[i], U3, V, nf)
    H_4, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f[i], U4, V, nf)
    H_5, n = Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f[i], U5, V, nf)

    # Test vector:
    phi = np.ones(n)
    phi_0 = phi/np.linalg.norm(phi)

    E_1, V_1, Q_T_1 = lanczos_iter(H_1, phi_0, k)
    E_2, V_2, Q_T_2 = lanczos_iter(H_2, phi_0, k)
    E_3, V_3, Q_T_3 = lanczos_iter(H_3, phi_0, k)
    E_4, V_4, Q_T_4 = lanczos_iter(H_4, phi_0, k)
    E_5, V_5, Q_T_5 = lanczos_iter(H_5, phi_0, k)

    # Ground state of the system in the original basis:
    state_1 = lin_comb_Q_T(V_1[:,0], Q_T_1)
    state_2 = lin_comb_Q_T(V_2[:,0], Q_T_2)
    state_3 = lin_comb_Q_T(V_3[:,0], Q_T_3)
    state_4 = lin_comb_Q_T(V_4[:,0], Q_T_4)
    state_5 = lin_comb_Q_T(V_5[:,0], Q_T_5)
    
    # Number of d-fermions:
    num_d_1.append(np.conj(state_1).dot(num_d_op.dot(state_1)))
    num_d_2.append(np.conj(state_2).dot(num_d_op.dot(state_2)))
    num_d_3.append(np.conj(state_3).dot(num_d_op.dot(state_3)))
    num_d_4.append(np.conj(state_4).dot(num_d_op.dot(state_4)))
    num_d_5.append(np.conj(state_5).dot(num_d_op.dot(state_5)))
    
    # Number of f-fermions:
    num_f_1.append(np.conj(state_1).dot(num_f_op.dot(state_1)))
    num_f_2.append(np.conj(state_2).dot(num_f_op.dot(state_2)))
    num_f_3.append(np.conj(state_3).dot(num_f_op.dot(state_3)))
    num_f_4.append(np.conj(state_4).dot(num_f_op.dot(state_4)))
    num_f_5.append(np.conj(state_5).dot(num_f_op.dot(state_5)))


    file_num_d_1.write('{}\t {}\n'.format(epsilon_f[i], num_d_1[i]))
    file_num_d_2.write('{}\t {}\n'.format(epsilon_f[i], num_d_2[i]))
    file_num_d_3.write('{}\t {}\n'.format(epsilon_f[i], num_d_3[i]))
    file_num_d_4.write('{}\t {}\n'.format(epsilon_f[i], num_d_4[i]))
    file_num_d_5.write('{}\t {}\n'.format(epsilon_f[i], num_d_5[i]))
    
    file_num_f_1.write('{}\t {}\n'.format(epsilon_f[i], num_f_1[i]))
    file_num_f_2.write('{}\t {}\n'.format(epsilon_f[i], num_f_2[i]))
    file_num_f_3.write('{}\t {}\n'.format(epsilon_f[i], num_f_3[i]))
    file_num_f_4.write('{}\t {}\n'.format(epsilon_f[i], num_f_4[i]))
    file_num_f_5.write('{}\t {}\n'.format(epsilon_f[i], num_f_5[i]))

    

file_num_d_1.close()
file_num_d_2.close()
file_num_d_3.close()
file_num_d_4.close()
file_num_d_5.close()

file_num_f_1.close()
file_num_f_2.close()
file_num_f_3.close()
file_num_f_4.close()
file_num_f_5.close()



# Plots:

plt.figure(figsize=(9, 7))

plt.ylabel("$<n_d>$", size = 18)
plt.xlabel("$\epsilon_f$", size = 18)

plt.scatter(epsilon_f, num_d_1, color='green', label = "U = {}".format(U1))
plt.plot(epsilon_f, num_d_1, color='green')

plt.scatter(epsilon_f, num_d_2, color='blue', label = "U = {}".format(U2))
plt.plot(epsilon_f, num_d_2, color='blue')

plt.scatter(epsilon_f, num_d_3, color='yellow', label = "U = {}".format(U3))
plt.plot(epsilon_f, num_d_3, color='yellow')

plt.scatter(epsilon_f, num_d_4, color='orange', label = "U = {}".format(U4))
plt.plot(epsilon_f, num_d_4, color='orange')

plt.scatter(epsilon_f, num_d_5, color='brown', label = "U = {}".format(U5))
plt.plot(epsilon_f, num_d_5, color='brown')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(loc='lower right', title = '$V = {}$'.format(V))

plt.savefig(my_path + '_FKM_num_d_diff_Ef_nf_{}_V_{}_L_{}.pdf'.format(nf, V, L))

plt.show()



plt.figure(figsize=(9, 7))

plt.ylabel("$<n_f>$", size = 18)
plt.xlabel("$\epsilon_f$", size = 18)

plt.scatter(epsilon_f, num_f_1, color='green', label = "U = {}".format(U1))
plt.plot(epsilon_f, num_f_1, color='green')

plt.scatter(epsilon_f, num_f_2, color='blue', label = "U = {}".format(U2))
plt.plot(epsilon_f, num_f_2, color='blue')

plt.scatter(epsilon_f, num_f_3, color='yellow', label = "U = {}".format(U3))
plt.plot(epsilon_f, num_f_3, color='yellow')

plt.scatter(epsilon_f, num_f_4, color='orange', label = "U = {}".format(U4))
plt.plot(epsilon_f, num_f_4, color='orange')

plt.scatter(epsilon_f, num_f_5, color='brown', label = "U = {}".format(U5))
plt.plot(epsilon_f, num_f_5, color='brown')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend(loc='upper right', title = '$V = {}$'.format(V))

plt.savefig(my_path + '_FKM_num_f_diff_Ef_nf_{}_V_{}_L_{}.pdf'.format(nf, V, L))

plt.show()


