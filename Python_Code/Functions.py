import numpy as np
import functools as ft
import cmath

from quspin.operators import hamiltonian
from quspin.basis import spinless_fermion_basis_1d


from numba import jit # To speed up the code

#%%

# ------------------------------------------------------------------

# Used methods:

# ------------------------------------------------------------------


#%% Discrete Fourier Transformation

def DFT(x_j):
    """
    ------------------
    x_j: Liste aller Punkte im Ortsraum
    ------------------
    """
    N = len(x_j) 
    summe = 0 # Auxiliary constant to calculate the x_k
    
    x_k = [] # List of points in k-space
    
    for k in range(N): # Calculation of the x_k
        for j in range(N):
            summe += x_j[j] * cmath.exp(- 2*np.pi * 1j * k * j / N) 
            
        x_k.append(summe)
        summe = 0
    return x_k

#%% Tridiagonal Matrix

def Tridiagonal(a, b, c):
    T = np.diag(a, -1) + np.diag(b, 0) + np.diag(c, 1)
    return T

#%% Lanczos algorithm

def Lanczos(phi_0, H, k):
    """
    ------------------
    phi_0: Test vector
    H: Hamiltonian of the considered system (sparse or dense)
    k: Number of iteration steps
    ------------------
    """

    # Lists for the calculated constants and the energies (eigenvalues)
    lst_a = []
    lst_b = []
    Energy = []
    
    
    # First iteration step:
    a = (phi_0.dot(H.dot(phi_0))) / (phi_0.dot(phi_0))
    phi_1 = H.dot(phi_0) - a * phi_0
    
    lst_a.append(a)
    
    Energy.append(a)
      
    for i in range(1, k): # further iteration steps
        a = (phi_1.dot(H.dot(phi_1))) / (phi_1.dot(phi_1))
        b2 = (phi_1.dot(phi_1)) / (phi_0.dot(phi_0))
        phi_new = (H.dot(phi_1)) - a * phi_1 - b2 * phi_0
        
        lst_b.append(np.sqrt(b2))
        lst_a.append(a)
        
        # Overwriting the phi's
        phi_0 = phi_1
        phi_1 = phi_new
        
        # Exit condition:
        E_n = np.linalg.eigvalsh(Tridiagonal(lst_b, lst_a, lst_b))[0]
        
        Energy.append(E_n)
        
    
    Eigenvalues, Eigenvectors = np.linalg.eigh(Tridiagonal(lst_b, lst_a, lst_b))
    Groundstate_lanc = Eigenvectors[:,0]

    return Energy, lst_a, lst_b, Groundstate_lanc


#%% Calculation of the ground state in the basis of the original Hamiltonian with the Lanczos algorithm

def Groundstate(phi_0, H, n, k):
    """
    ------------------
    phi_0: Test vector
    H: Hamiltonian of the considered system
    n: Dimension
    k: Number of iteration steps
    ------------------
    """
    # Results of the Lanczos algorithm
    Lanc = Lanczos(phi_0, H, k)
    lst_a = Lanc[1]
    lst_b = Lanc[2]
    Groundstate_lanc = Lanc[3]

    groundstate = 0
    
    # Reconstruction of the Lanczos vectors and simultaneous calculation of the ground state:
    phi_1 = H.dot(phi_0) - lst_a[0] * phi_0
    groundstate += Groundstate_lanc[0] * phi_0/np.linalg.norm(phi_0)
    groundstate += Groundstate_lanc[1] * phi_1/np.linalg.norm(phi_1)

    for i in range(1, k-1):
        phi_new = H.dot(phi_1) - lst_a[i] * phi_1 - lst_b[i-1]**2 * phi_0
        groundstate += Groundstate_lanc[i+1] * phi_new/np.linalg.norm(phi_new)
        
        # Overwriting the phi's
        phi_0 = phi_1
        phi_1 = phi_new
    
    # Normalization of the ground state:
    groundstate = groundstate/np.linalg.norm(groundstate)

    return groundstate

#%% Time evolution ED

def Time_evolution_ED(H, groundstate, t):
    """
    ------------------
    H: Hamiltonian of the considered system
    groundstate: Time independent ground state
    t: Time
    ------------------
    """
    
    state_t = 0
    
    Energies, states = np.linalg.eigh(H)    
    Energies_Exp = np.exp(-(1j) * t * Energies)
    
    # Time evolution of the ground state at time t:
    for i in range(len(Energies)):
        state_t += Energies_Exp[i] * np.matmul(np.conj(states[:,i]), groundstate) * states[:,i]
    
    return state_t


#%% Gram-Schmidt process

def Gramschmidt(V):
    """
    ------------------
    V: Matrix of the vectors, which have to be reorthogonalized
    ------------------
    """
    
    n = V.shape[1] # Number of vectors
    
    V_copy = V # Matrix for the renormalized vectors
    
    for i in range(1, n):
        for j in range(i):
            
            V_copy[:,i] -= (np.matmul(np.conj(V_copy[:,j]), V[:,i]))/(np.matmul(np.conj(V_copy[:,j]), V_copy[:,j])) * V_copy[:,j]
       
    return V_copy

#%% Time evolution with the Lanczos algorithm

def Time_evolution_Lanczos(k, H, state, dt, n):
    """
    ------------------
    k: Number of iteration steps
    H: Hamiltonian of the considered system
    state: Ground state of the system
    dt: Time step
    n: Dimension of the system
    ------------------
    """
    
    # Auxilary lists:
    V = np.zeros((n,k-1), dtype = np.complex128)
    lst_a = []
    lst_b = []

    phi_0 = state
    a = np.conj(phi_0).dot(H.dot(phi_0)) / (np.conj(phi_0).dot(phi_0))
    phi_1 = H.dot(phi_0) -  a * phi_0
    
    lst_a.append(a)

    V[:,0] = phi_0/np.linalg.norm(phi_0)
    V[:,1] = phi_1/np.linalg.norm(phi_1)

    for i in range(2, k):
        a = np.conj(phi_1).dot(H.dot(phi_1)) / np.conj(phi_1).dot(phi_1)
        b = np.conj(phi_1).dot(phi_1) / np.conj(phi_0).dot(phi_0)

        lst_a.append(a)
        lst_b.append(np.sqrt(b))

        
        phi_new = H.dot(phi_1) - a*phi_1 - b * phi_0
        
        # Overwriting the phi's
        phi_0 = phi_1
        phi_1 = phi_new

        if(i < k - 1):
            V[:,i] = phi_new/np.linalg.norm(phi_new)
            
    V = Gramschmidt(V) # Reorthogonalisation of the Lanczos vectors
    
    
    T = Tridiagonal(lst_b, lst_a, lst_b) # Tridiagonal matrix
    Eigen, Q = np.linalg.eigh(T) 

    # Calculating the diagonal matrix:
    Eigen_exp = np.exp(-1j * dt * Eigen)
    D = np.diag(Eigen_exp) 
    
    # Matrix exponential of the tridiagonal matrix:
    T_exp = Q.dot(D.dot(np.conjugate(np.transpose(Q))))
    
    # Time evolution operator:
    U = V.dot(T_exp.dot(np.conjugate(np.transpose(V))))

    state_t = U.dot(state)

    return state_t

#%% Optimized time evolution with the Lanczos algorithm

@jit
def Time_evolution_Lanczos_opt(k, H, state, dt, n):
    """
    ------------------
    k: Number of iteration steps
    H: Hamiltonian of the considered system
    state: Ground state of the system
    dt: Time step
    n: Dimension of the system
    ------------------
    """
    
    # Auxilary lists:
    V = np.zeros((n,k-1), dtype = np.complex128)
    lst_a, lst_b = [], []

    phi_0 = state
    a = np.conj(phi_0).dot(H.dot(phi_0))/(np.conj(phi_0).dot(phi_0))
    phi_1 = H.dot(phi_0) -  a * phi_0
    
    lst_a.append(a)

    V[:,0], V[:,1] = phi_0/np.linalg.norm(phi_0), phi_1/np.linalg.norm(phi_1)

    for i in range(2, k):
        a = np.conj(phi_1).dot(H.dot(phi_1))/np.conj(phi_1).dot(phi_1)
        b = np.conj(phi_1).dot(phi_1)/np.conj(phi_0).dot(phi_0)

        lst_a.append(a)
        lst_b.append(np.sqrt(b))

        phi_new = H.dot(phi_1) - a*phi_1 - b * phi_0
        
        # Overwriting the phi's
        phi_0, phi_1 = phi_1, phi_new

        if(i < k - 1):
            V[:,i] = phi_new/np.linalg.norm(phi_new)
            
    V = Gramschmidt(V) # Reorthogonalisation of the Lanczos vectors
    
    
    T = Tridiagonal(lst_b, lst_a, lst_b) # Tridiagonal matrix
    Eigen, Q = np.linalg.eigh(T) 

    # Calculating the diagonal matrix:
    Eigen_exp = np.exp(-1j * dt * Eigen)
    D = np.diag(Eigen_exp) 
    
    state_t = V.dot(Q.dot(D.dot(Q[0])))

    return state_t


#%%

# ------------------------------------------------------------------

# Hamiltonian and Operators for the Ising model:

# ------------------------------------------------------------------

#%% Function for all spin operators for a number of lattice sites L

def spin_operator(L, sigma):
    """
    ------------------
    L: Number of lattice sites
    sigma: Considered pauli matrix
    ------------------
    """

    sigma_i = []
    
    for i in range(1, L+1): # Loop for the spin operators on lattice side i
        # Lists to determine the spin operator
        lst = []
        
        for k in range(i-1):
            lst.append(np.identity(2))
  
        lst.append(sigma)
        
        for n in range(L-i):
            lst.append(np.identity(2))
                 
            
        # Saving the spin operators
        sigma_i.append(ft.reduce(np.kron, lst))
       
    return sigma_i

#%% Ising-Hamiltonian

def Hamiltonian_Ising(L, h):
    """
    ------------------
    L: Number of lattice sites
    h: Strength of the magnetic field
    ------------------
    """
   
    summe_1 = 0 # First sum of the Hamiltonian
    summe_2 = 0 # Second sum of the Hamiltonian
    
    # Pauli matrices:
    sigma_z = np.array([[1, 0], 
                        [0, -1]])
    sigma_x = np.array([[0, 1], 
                        [1, 0]])
    
    # Lists for the spin operators
    sigma_x_i = spin_operator(L, sigma_x)
    sigma_z_i = spin_operator(L, sigma_z)

    for i in range(L-1):
        summe_1 += np.matmul(sigma_z_i[i], sigma_z_i[i+1])
    for i in range(L):    
        summe_2 += sigma_x_i[i]
     
        
    H = - summe_1 + h * summe_2  
    return H

#%% Calculation of the total magnetization

def Magnetization_total(State, L, sigma):
    """
    ------------------
    State: Considered state
    L: Number of lattice sides
    sigma: Pauli matrix
    ------------------
    """
    
    summe = 0 # Auxilary variable for calculating the sum
    sigma_i = spin_operator(L, sigma)
    
    for i in range(L): # Calculating the eigenvalues of the spin operators
                
        summe += np.matmul(np.conj(State), np.matmul(sigma_i[i], State))
    
    # Calculating the magnetization:
    m =  summe / L
    
    return m

#%% Calculation of the local magnetization

def Magnetization_local(phi_0, L, h, epsilon, sigma):
    """
    ------------------
    phi_0: Test vector
    L: Number of lattice sides
    h: Strength of the magnetic field
    epsilon: Desired accuracy of the method
    sigma: Pauli matrix
    ------------------
    """    
    m_i = [] # List for the local magnetization
    
    # Calculating the Hamiltonian and the ground state:
    n = 2**L
    H = Hamiltonian_Ising(L, h)
    State = Groundstate(phi_0, H, epsilon, n)
    
    for L_i in range(1, L+1): # Loop for the spin operators on site i
            
        # Calculating the spin operators
        sigma_i = spin_operator(L_i, sigma)
       
        # Calculating the expectation value of the individual spin operators
        m_i.append(np.matmul(State, np.matmul(sigma_i, State)))
    
    return m_i

#%%

# ------------------------------------------------------------------

# Hamiltonian and Operators for the FKM:

# ------------------------------------------------------------------


#%% Modelling of the extended FKM in one layer system 

def Hamiltonian_FKM_linear(L, t_d, t_f, epsilon_d, epsilon_f, U, V, nf):
    """
    ------------------
    L: Number of lattice sites
    n_d: Particle density of the d-electrons
    n_f: Particle density of the f-electrons
    t_d: Hopping amplitude of the d-electrons
    t_f: Hopping amplitude of the f-electrons
    epsilon_d: Potential energy of the d-electrons
    epsilon_f: Potential energy of the f-electrons    
    U: On-site Coulomb interaction
    V: Hybridization
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)
    
    # For open boundary conditions:
    hopping_d = [[t_d, i, (i + 2)] for i in range(L-3) if i%2 == 0]
    hopping_d_hc = [[-np.conj(t_d), i, (i + 2)] for i in range(L-3) if i%2 == 0]
    
    hopping_f = [[t_f, i, (i + 2)] for i in range(L-2) if i%2 != 0]
    hopping_f_hc = [[-np.conj(t_f), i, (i + 2)] for i in range(L-2) if i%2 != 0]
    
    pot_d = [[epsilon_d, i] for i in range(L) if i%2 == 0]
    pot_f = [[epsilon_f, i] for i in range(L) if i%2 != 0]
    
    interaction = [[U, i, i+1] for i in range(L-1) if i%2 == 0]
    hybrid = [[V, i, i+1] for i in range(L-1) if i%2 == 0]
    hybrid_hc = [[-V, i, i+1] for i in range(L-1) if i%2 == 0]
    
    # Construction of the Hamiltonian with static and dynamic list:
    static = [["n", pot_d], ["+-", hopping_d], ["-+", hopping_d_hc], 
              ["n", pot_f], ["+-", hopping_f], ["-+", hopping_f_hc],
              ["nn", interaction], 
              ["+-", hybrid], ["-+", hybrid_hc]]
    dynamic = []
    
    H = hamiltonian(static, dynamic, dtype=np.complex128, basis = basis, check_symm=False)
    
    # Return Hamiltonian and the dimension of the Hilbert space:
    return H, basis.Ns

#%% FKM-Hamiltonian with electric field

def Hamiltonian_FKM_electric(L, t_d, t_f, epsilon_d, epsilon_f, U, V, E, nf):
    """
    ------------------
    L: Number of lattice sites
    n_d: Particle density of the d-electrons
    n_f: Particle density of the f-electrons
    t_d: Hopping amplitude of the d-electrons
    t_f: Hopping amplitude of the f-electrons
    epsilon_d: Potential energy of the d-electrons
    epsilon_f: Potential energy of the f-electrons    
    U: On-site Coulomb interaction
    V: Hybridization
    E: Electric field
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)
    
    # For open boundary conditions:
    hopping_d = [[t_d, i, (i + 2)] for i in range(L-3) if i%2 == 0]
    hopping_d_hc = [[-np.conj(t_d), i, (i + 2)] for i in range(L-3) if i%2 == 0]
    
    hopping_f = [[t_f, i, (i + 2)] for i in range(L-2) if i%2 != 0]
    hopping_f_hc = [[-np.conj(t_f), i, (i + 2)] for i in range(L-2) if i%2 != 0]
    
    pot_d = [[epsilon_d, i] for i in range(L) if i%2 == 0]
    pot_f = [[epsilon_f, i] for i in range(L) if i%2 != 0]
    
    interaction = [[U, i, i+1] for i in range(L-1) if i%2 == 0]
    hybrid = [[V, i, i+1] for i in range(L-1) if i%2 == 0]
    hybrid_hc = [[-V, i, i+1] for i in range(L-1) if i%2 == 0]
    
    x_mid = L/4 - 0.5
    
    i_lst =[]
    for j in range(L//2):
        i_lst.append(j)
        i_lst.append(j)

    electric_d = [[-E * (i_lst[i] - x_mid), i] for i in range(L) if i%2 == 0] 
    electric_f = [[-E * (i_lst[i] - x_mid), i] for i in range(L) if i%2 != 0] 
    
    # Construction of the Hamiltonian with static and dynamic list:
    static = [["n", pot_d], ["+-", hopping_d], ["-+", hopping_d_hc], 
              ["n", pot_f], ["+-", hopping_f], ["-+", hopping_f_hc],
              ["nn", interaction], 
              ["+-", hybrid], ["-+", hybrid_hc],
              ["n", electric_d], ["n", electric_f]]
    dynamic = []
    
    H = hamiltonian(static, dynamic, dtype=np.complex128, basis = basis, check_symm=False)
    
    # Return Hamiltonian and the dimension of the Hilbert space:
    return H, basis.Ns

#%% Polarization operator of the linear modelled FKM 

def Polarization_FKM_linear(L, nf):
    """
    ------------------
    L: Number of lattice sites
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)

    polar = [[1/L, i, i+1] for i in range(L-1) if i%2 == 0] 
    polar_hc = [[-1/L, i, i+1] for i in range(L-1) if i%2 == 0]
    
    # Construction of the Operator with static and dynamic list:
    static = [["+-", polar], ["-+", polar_hc]]
    dynamic = []
    
    P = hamiltonian(static, dynamic, dtype=np.float64, basis = basis, check_symm = False, check_pcon = False)
    
    return P

#%% Polarization operator of the linear modelled FKM with variable mu

def Polarization_FKM_linear_mu(L, nf, mu):
    """
    ------------------
    L: Number of lattice sites
    nf: Density of fermions
    mu: inter-band dipole matrix element
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)

    polar = [[mu/L, i, i+1] for i in range(L-1) if i%2 == 0] 
    polar_hc = [[-mu/L, i, i+1] for i in range(L-1) if i%2 == 0]
    
    # Construction of the Operator with static and dynamic list:
    static = [["+-", polar], ["-+", polar_hc]]
    dynamic = []
    
    P = hamiltonian(static, dynamic, dtype=np.float64, basis = basis, check_symm = False, check_pcon = False)
    
    return P


#%% Charge location operator of the linear modelled FKM 

def Charge_location_Polar_linear(L, nf):
    """
    ------------------
    L: Number of lattice sites
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)
    
    x_mid = L/4 - 0.5
    
    i_lst =[]
    for j in range(L//2):
        i_lst.append(j)
        i_lst.append(j)

    charge_d = [[(i_lst[i] - x_mid)/L, i] for i in range(L) if i%2 == 0] 
    charge_f = [[(i_lst[i] - x_mid)/L, i] for i in range(L) if i%2 != 0] 

    # Construction of the Operator with static and dynamic list:
    static = [["n", charge_d], ["n", charge_f]]
    dynamic = []
    
    return hamiltonian(static, dynamic, dtype=np.float64, basis = basis, check_symm=False)


#%% d-fermion number operator of the linear modelled FKM 

def Fermion_d_number_linear(L, nf):
    """
    ------------------
    L: Number of lattice sites
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)

    loc = [[2/L, i] for i in range(L) if i%2 == 0]  
    
    # Construction of the Operator with static and dynamic list:
    static = [["n", loc]]
    dynamic = []
    
    return hamiltonian(static, dynamic, dtype=np.float64, basis = basis, check_symm=False)

 
#%% f-fermion number operator of the linear modelled FKM 

def Fermion_f_number_linear(L, nf):
    """
    ------------------
    L: Number of lattice sites
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)

    loc = [[2/L, i] for i in range(L) if i%2 != 0]  
    
    # Construction of the Operator with static and dynamic list:
    static = [["n", loc]]
    dynamic = []
    
    return hamiltonian(static, dynamic, dtype=np.float64, basis = basis, check_symm=False)


#%% Function to calculate the local fermion numbers of the linear modelled FKM 

def Local_fermion_number_linear(L, nf, state):
    """
    ------------------
    L: Number of lattice sites
    nf: Density of fermions
    state: Considered state
    ------------------
    """
    
    local_density_d = []
    local_density_f = []
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)
    
    dynamic = []
    
    for i in range(L):
        if i%2 == 0: # for d-fermions
        
            # Construction of the Operator:
            loc_d = [[1, i]] 
            static_d = [["n", loc_d]]
            numb_d = hamiltonian(static_d, dynamic, dtype=np.float64, basis = basis, check_symm=False)
            
            # Expectation value:
            local_density_d.append(np.conj(state).dot(numb_d.dot(state)))
            
        else: # for f-fermions
            
            # Construction of the Operator:
            loc_f = [[1, i]] 
            static_f = [["n", loc_f]]
            numb_f = hamiltonian(static_f, dynamic, dtype=np.float64, basis = basis, check_symm=False)
            
            # Expectation value:
            local_density_f.append(np.conj(state).dot(numb_f.dot(state)))

    return local_density_d, local_density_f

#%% Operators for the photocurrent

def current_d(t_d, L, nf):
    """
    ------------------
    t_d: Hopping amplitude of the d-electrons
    L: Number of lattice sites
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)

    current_d = [[1j * t_d, (i+2), i] for i in range(L-3) if i%2 == 0]
    current_d_hc = [[1j * np.conj(t_d), (i+2), i] for i in range(L-3) if i%2 == 0]
    
    # Construction of the Operator with static and dynamic list:
    static = [["+-", current_d], ["-+", current_d_hc]]
    dynamic = []
    
    return hamiltonian(static, dynamic, dtype = np.complex128, basis = basis, check_symm=False)


def current_f(t_f, L, nf):
    """
    ------------------
    t_f: Hopping amplitude of the f-electrons
    L: Number of lattice sites
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)

    current_f = [[1j * t_f, (i+2), i] for i in range(L-2) if i%2 != 0] 
    current_f_hc = [[1j * np.conj(t_f), (i+2), i] for i in range(L-2) if i%2 != 0] 
    
    # Construction of the Operator with static and dynamic list:
    static = [["+-", current_f], ["-+", current_f_hc]]
    dynamic = []
    
    return hamiltonian(static, dynamic, dtype = np.complex128, basis = basis, check_symm=False)


def current_df(V, L, nf):
    """
    ------------------
    V: Hybridization
    L: Number of lattice sites
    nf: Density of fermions
    ------------------
    """
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)

    current_df = [[1j * V, i, (i+1)] for i in range(L-1) if i%2 == 0] 
    current_df_hc = [[1j * np.conj(V), i, (i+1)] for i in range(L-1) if i%2 == 0] 
    
    # Construction of the Operator with static and dynamic list:
    static = [["+-", current_df], ["-+", current_df_hc]]
    dynamic = []
    
    return hamiltonian(static, dynamic, dtype = np.complex128, basis = basis, check_symm=False)

#%% Local current

def local_current(t_d, t_f, V, L, nf, state):
    """
    ------------------
    t_d: Hopping amplitude of the d-electrons
    t_f: Hopping amplitude of the f-electrons
    V: Hybridization
    L: Number of lattice sites
    nf: Density of fermions
    state: Considered state
    ------------------
    """
    
    local_current_d = []
    local_current_f = []
    local_current_df = []
    
    # Basis of the Hilbert space:
    basis = spinless_fermion_basis_1d(L, nf = nf)
    
    dynamic = []
    
    
    # d-current:
    for i in range(L-3):
        if(i%2 == 0):
            current_d = [[1j * t_d, (i+2), i]]
            current_d_hc = [[1j * np.conj(t_d), (i+2), i]]
            
            static_d = [["+-", current_d], ["-+", current_d_hc]]
            
            # Operator for d-fermions:
            local_j_op_d = hamiltonian(static_d, dynamic, dtype=np.complex128, basis = basis, check_symm=False)
            
            # Expectation value:
            local_current_d.append(np.conj(state).dot(local_j_op_d.dot(state)))
        
        
    # f-current:    
    for i in range(L-2):
        if(i%2 != 0):
            current_f = [[1j * t_f, (i+2), i]] 
            current_f_hc = [[1j * np.conj(t_f), (i+2), i]]  
        
            # Construction of the Operator with static and dynamic list:
            static_f = [["+-", current_f], ["-+", current_f_hc]]
        
            # Operator:
            local_j_op_f = hamiltonian(static_f, dynamic, dtype=np.complex128, basis = basis, check_symm=False)
            
            # Expectation value:
            local_current_f.append(np.conj(state).dot(local_j_op_f.dot(state)))
    
        
    # df-current:   
    for i in range(L-1):
        if(i%2 == 0):
            current_df = [[1j * V, (i+1), i]] 
            current_df_hc = [[1j * np.conj(V), (i+1), i]]  
        
            # Construction of the Operator with static and dynamic list:
            static_df = [["+-", current_df], ["-+", current_df_hc]]
        
            # Operator:
            local_j_op_df = hamiltonian(static_df, dynamic, dtype=np.complex128, basis = basis, check_symm=False)
            
            # Expectation value:
            local_current_df.append(np.conj(state).dot(local_j_op_df.dot(state)))
        
        
    return local_current_d, local_current_f, local_current_df

