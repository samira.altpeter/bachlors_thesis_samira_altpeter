import sys 
# Path of the Functions file:
sys.path.append(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code")
from Functions import Lanczos, Hamiltonian_Ising

import numpy as np
import matplotlib.pyplot as plt


import os
my_path = os.path.abspath(__file__)

#%% Parameters

# Strength of the magnetic field:
h = 5. 


# Number of iteration steps for the Lanczos algorithm:
k = 60


# Considered number of lattice sites:
L_5 = 5 # Number of lattice sites
n_5 = 2**L_5 # Dimension

L_6 = 6 # Number of lattice sites
n_6 = 2**L_6 # Dimension

L_7 = 7 # Number of lattice sites
n_7 = 2**L_7 # Dimension

L_8 = 8 # Number of lattice sites
n_8 = 2**L_8 # Dimension

L_9 = 9 # Number of lattice sites
n_9 = 2**L_9 # Dimension

L_10 = 10 # Number of lattice sites
n_10 = 2**L_10 # Dimension

#%% Convergence of the Lanczos algorithm for different numbers of lattice sites

# Files for saving the data:
file_5 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/Ising model/Convergence/Convergence_data_h_{}_L_{}.txt".format(h, L_5), 'w')
file_6 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/Ising model/Convergence/Convergence_data_h_{}_L_{}.txt".format(h, L_6), 'w')
file_7 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/Ising model/Convergence/Convergence_data_h_{}_L_{}.txt".format(h, L_7), 'w')
file_8 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/Ising model/Convergence/Convergence_data_h_{}_L_{}.txt".format(h, L_8), 'w')
file_9 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/Ising model/Convergence/Convergence_data_h_{}_L_{}.txt".format(h, L_9), 'w')
file_10 = open(r"C:/Users/samal/OneDrive/Dokumente/Uni/Bachelor/Semester 8/Bachelorarbeit/Python-Code/Ising model/Convergence/Convergence_data_h_{}_L_{}.txt".format(h, L_10), 'w')


k_list = np.arange(1, k+1, 1)


phi_0_5 = np.ones(n_5) # Test vector
H_5 = Hamiltonian_Ising(L_5, h) # Calculation of the Hamiltonian

E_5 = Lanczos(phi_0_5, H_5, k)[0] # Energies for every iteration step
E0_5 = Lanczos(phi_0_5, H_5, k)[0][k-1] # Ground state energy

for i in range(len(E_5)):
    file_5.write('{}\t {}\n'.format(i + 1, E_5[i]))


phi_0_6 = np.ones(n_6) # Test vector
H_6 = Hamiltonian_Ising(L_6, h) # Calculation of the Hamiltonian

E_6 = Lanczos(phi_0_6, H_6, k)[0] # Energies for every iteration step
E0_6 = Lanczos(phi_0_6, H_6, k)[0][k-1] # Ground state energy

for i in range(len(E_6)):
    file_6.write('{}\t {}\n'.format(i + 1, E_6[i]))
    

phi_0_7 = np.ones(n_7) # Test vector
H_7 = Hamiltonian_Ising(L_7, h) # Calculation of the Hamiltonian

E_7 = Lanczos(phi_0_7, H_7, k)[0] # Energies for every iteration step
E0_7 = Lanczos(phi_0_7, H_7, k)[0][k-1] # Ground state energy

for i in range(len(E_7)):
    file_7.write('{}\t {}\n'.format(i + 1, E_7[i]))
    

phi_0_8 = np.ones(n_8) # Test vector
H_8 = Hamiltonian_Ising(L_8, h) # Calculation of the Hamiltonian

E_8 = Lanczos(phi_0_8, H_8, k)[0] # Energies for every iteration step
E0_8 = Lanczos(phi_0_8, H_8, k)[0][k-1] # Ground state energy

for i in range(len(E_8)):
    file_8.write('{}\t {}\n'.format(i + 1, E_8[i]))


phi_0_9 = np.ones(n_9) # Test vector
H_9 = Hamiltonian_Ising(L_9, h) # Calculation of the Hamiltonian

E_9 = Lanczos(phi_0_9, H_9, k)[0] # Energies for every iteration step
E0_9 = Lanczos(phi_0_9, H_9, k)[0][k-1] # Ground state energy

for i in range(len(E_9)):
    file_9.write('{}\t {}\n'.format(i + 1, E_9[i]))
    

phi_0_10 = np.ones(n_10) # Test vector
H_10 = Hamiltonian_Ising(L_10, h) # Calculation of the Hamiltonian

E_10 = Lanczos(phi_0_10, H_10, k)[0] # Energies for every iteration step
E0_10 = Lanczos(phi_0_10, H_10, k)[0][k-1] # Ground state energy

for i in range(len(E_10)):
    file_10.write('{}\t {}\n'.format(i + 1, E_10[i]))


file_5.close()
file_6.close()
file_7.close()
file_8.close()
file_9.close()
file_10.close()



# Plot:

plt.figure(figsize=(9, 7))

plt.ylabel("Energy difference $E_i - E_0$", size = 18)
plt.xlabel("Iteration step $k$", size = 18)

plt.scatter(k_list, E_5 - E0_5,color='blue', label = "$L=5$", marker='o')
plt.scatter(k_list, E_6 - E0_6,color='yellow', label = "$L=6$", marker='x')
plt.scatter(k_list, E_7 - E0_7,color='cyan', label = "$L=7$", marker='*')
plt.scatter(k_list, E_8 - E0_8,color='magenta', label = "$L=8$", marker='s')
plt.scatter(k_list, E_9 - E0_9,color='orange', label = "$L=9$", marker='v')
plt.scatter(k_list, E_10 - E0_10,color='green', label = "$L=10$", marker='+')

plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.legend()

plt.savefig(my_path + '_Convergence_h_{}.pdf'.format(h))

plt.show()
    
